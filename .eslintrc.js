module.exports = {
	root: true,
	plugins: ['@typescript-eslint'],
	extends: ['eslint-config-standard'],
	parser: '@typescript-eslint/parser',
	env: {
		node: true,
		es6: true
	},
	rules: {
		'no-console': process.env.PRE_COMMIT || process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.PRE_COMMIT || process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-tabs': 'off',
		'max-len': ['error', 150],
		indent: ['error', 'tab'],
		semi: ['error', 'never'],
		quotes: ['error', 'single'],
		'comma-dangle': 'off',
		'object-curly-spacing': ['error', 'always'],
		'arrow-parens': ['error', 'as-needed'],
		'array-element-newline': ['error', 'consistent'],
		'space-before-function-paren': ['error', { "anonymous": "always", "named": "never", "asyncArrow": "always" }],
		'no-trailing-spaces': ['error', { skipBlankLines: true }],
		'no-undef': 'off',
		'no-unused-vars': 'off',
		strict: 'off'
	}
}
