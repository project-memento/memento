FROM cypress/base:10

WORKDIR /usr/src/app

ARG CYPRESS_VERSION="3.3.0"

RUN yarn global add "cypress@${CYPRESS_VERSION}"
RUN cypress verify
RUN cypress install

# Cypress cache and installed version
RUN cypress cache path
RUN cypress cache list

COPY ./package.json ./yarn.lock ./
COPY ./backend/package.json ./backend/yarn.lock ./backend/
COPY ./frontend/package.json ./frontend/.npmrc ./frontend/yarn.lock ./frontend/

RUN yarn

COPY . .

RUN yarn build

EXPOSE 8080

CMD ["yarn", "start"]
