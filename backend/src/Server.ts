import * as path from 'path'
import { GlobalAcceptMimesMiddleware, Next, Request, Response, ServerLoader, ServerSettings } from '@tsed/common'
import * as cookieParser from 'cookie-parser'
import * as compress from 'compression'
import * as methodOverride from 'method-override'
import * as bodyParser from 'body-parser'
import * as history from 'connect-history-api-fallback'
import * as passport from 'passport'
import { IGraphQLSettings } from '@tsed/graphql'
import { typeorm } from './TypeOrm'
import { StrategyOptions } from 'passport-jwt'
import { UserService } from './services/UserService'
import * as jwt from 'jsonwebtoken'
import * as Express from 'express'
import { Forbidden } from 'ts-httpexceptions'
import { existsSync, mkdirSync } from 'fs'
import * as cors from 'cors'
import * as WebSocket from 'ws'
import { ConnectionContext } from 'subscriptions-transport-ws'
import { azureStorage, DiskStorage } from './extensions/multer'
import { getConfiguredRedisPubSub } from './extensions/graphql/pubsub'
import * as os from 'os'
import { ErrorInterceptor } from './graphql/ErrorMiddleware'
import { v4 } from 'uuid'

const rootDir = __dirname
const publicDir = path.join(
	rootDir,
	process.env.NODE_ENV === 'production' ? '/../../../public/' : '/../public/'
)

const jwtSecret = v4()

@ServerSettings({
	rootDir,
	publicDir,
	httpPort: 8081,
	httpsPort: false,
	acceptMimes: ['application/json', 'multipart/form-data'],
	componentsScan: [
		'${rootDir}/middlewares/**/*.{js,ts}',
		'${rootDir}/services/**/*.{js,ts}',
		'${rootDir}/graphql/**/*.{js,ts}',
		'${rootDir}/converters/**/*.{js,ts}',
		'${rootDir}/extensions/**/*.{js,ts}',
	],
	mount: {
		'/': [
			'${rootDir}/api/v1/**/*.{js,ts}'
		]
	},
	azureBlobStorage: {
		accountName: process.env.BLOB_ACCOUNT_NAME,
		accountKey: process.env.BLOB_ACCOUNT_KEY,
		containerName: process.env.BLOB_CONTAINER_NAME,
	},
	statics: {
		'/': `${rootDir}/uploads/**/*`
	},
	graphql: {
		default: {
			path: '/graphql',
			resolvers: ['${rootDir}/graphql/resolvers/**/*.ts'],
			buildSchemaOptions: {
				emitSchemaFile: './schema.graphql',
				authChecker: ({ root, args, context, info }, roles,) => {
					if (context.user === undefined) throw new Forbidden('Not logged in')
					return true
				},
				resolvers: ['${rootDir}/graphql/resolvers/**/*.ts'],
				globalMiddlewares: [
					ErrorInterceptor
				]
			},
			serverConfig: {
				context: ({ req, connection }) => {
					const reqOrConnection = req || connection.context
					return { user: reqOrConnection.user }
				},
				subscriptions: {
					onConnect: (connectionParams, websocket, context) => context,
				}
			},
			installSubscriptionHandlers: true
		} as IGraphQLSettings
	},
	typeorm: [typeorm],
	jwt: {
		secretOrKey: jwtSecret,
		issuer: 'connect.app',
		audience: 'connect.app'
	} as Partial<StrategyOptions>

})
export class Server extends ServerLoader {
	constructor() {
		super()
		this.use('/api/**', (req: Request, res: Response, next: Next) => {
			passport.authenticate('jwt', { session: false }, (_, user, __, ___) => {
				if (user) {
					req.user = user
				}
				next()
			})(req, res, next)
		})
		this.use('/graphql', (req: Request, res: Response, next: Next) =>
			passport.authenticate('jwt', { session: false }, (_, user, __, ___) => {
				if (user) {
					req.user = user
				}
				next()
			})(req, res, next)
		)
		const injector = this.injector
		/*
		getConfiguredRedisPubSub().then(pubsub => {
			this.settings.set('graphql.default.buildSchemaOptions.pubSub', { eventEmitter: pubsub.ee })
		})
		*/
		this.settings.set('graphql.default.serverConfig.subscriptions.onConnect',
			async (connectionParams: any, websocket: WebSocket, context: ConnectionContext) => {
				websocket.send(JSON.stringify({ type: 'connection_ack', hostname: os.hostname() }))
				if (connectionParams.authorization) {
					const userService : UserService = await injector.get(UserService) as UserService
					const regex = /bearer (.+)/i
					if (!regex.test(connectionParams.authorization)) return context
					// @ts-ignore
					const token = regex.exec(connectionParams.authorization)[1]
					return new Promise(resolve => {
						jwt.verify(token, jwtSecret, async (_, decoded: any) => {
							try {
								const user = await userService.find({ id: decoded.sub })
								if (!user) {
									websocket.close(4001, JSON.stringify({ cause: 'Invalid Token' }))
								}
								resolve({ ...context, user })
							} catch (_) {
								resolve(context)
							}
						})
					})
				}
				return context
			})

		const storage = this.settings.get('azureBlobStorage.accountName') !== undefined
			? azureStorage(this.settings.get('azureBlobStorage'))
			: DiskStorage(publicDir)
		this.settings.set('multer', { storage })
	}

	public $onMountingMiddlewares() {
		this.use(GlobalAcceptMimesMiddleware)
			.use(history())
			.use(cookieParser())
			.use(compress())
			.use(methodOverride())
			.use(bodyParser.json())
			.use(passport.initialize())
			.use(
				bodyParser.urlencoded({
					extended: true
				})
			)
			.use(cors())
			.use(Express.static(publicDir, { fallthrough: true }))

		return null
	}

	public $onReady() {
		console.log('Server started...')
	}

	public $onServerInitError(error: any): any {
		console.error(error)
	}
}
