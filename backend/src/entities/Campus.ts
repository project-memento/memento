import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { School, SchoolInput } from './School'
import { User } from './User'
import { Lazy } from '../helpers/Lazy'
import { Appointment } from './Appointment'

@Entity()
@ObjectType()
export class Campus {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	@Field(type => String)
	name!: string

	@Column()
	@Field()
	address!: string

	@ManyToOne(type => School, school => school.campuses)
	@Field(type => School)
	school!: Lazy<School>

	@OneToMany(type => Appointment, appointment => appointment.location, { lazy: true })
	@Field(type => [Appointment])
	appointments!: Lazy<Appointment>[]

	@ManyToMany(type => User, user => user.campuses, { lazy: true })
	@JoinTable()
	@Field(type => User)
	students!: User[]

	@Column({ nullable: true })
	@Field({ nullable: true })
	deletedAt?: Date

	constructor(name: string|undefined = undefined, address: string|undefined = undefined, school: School|undefined = undefined) {
		if (name !== undefined) this.name = name
		if (school !== undefined) this.school = school
	}
}

@InputType()
export class CampusInput implements Partial<Campus> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field({ nullable: true })
	name!: string

	@Field({ nullable: true })
	address!: string

	@Field(type => SchoolInput, { nullable: true })
	school!: School

	@Field({ nullable: true })
	deletedAt!: Date
}
