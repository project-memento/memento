import { Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, ID, Field } from 'type-graphql'
import { Message } from './Message'
import { Lazy } from '../helpers/Lazy'
import { ConversationParticipant } from './ConversationParticipant'
import { User } from './User'

@Entity()
@ObjectType()
export class Conversation {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@OneToMany(type => ConversationParticipant, participant => participant.conversation, { eager: true, cascade: true })
	@Field(type => [ConversationParticipant])
	participants!: ConversationParticipant[]

	@OneToMany(type => Message, message => message.conversation, { lazy: true, cascade: ['remove'] })
	@Field(type => [Message])
	messages!: Lazy<Message[]>

	@Field(type => Message, { nullable: true })
	lastMessage!: Message|null

	constructor(participants: (ConversationParticipant|User)[]|undefined = undefined) {
		if (participants !== undefined) {
			this.participants = participants.map(participant => {
				if (participant instanceof ConversationParticipant) {
					return participant
				}
				return new ConversationParticipant(undefined, participant)
			})
		}
	}
}
