import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { EmailSuffix, EmailSuffixInput } from './EmailSuffix'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { Lazy } from '../helpers/Lazy'
import { Campus, CampusInput } from './Campus'
import { User } from './User'
import { FieldOfStudy } from './FieldOfStudy'

@Entity()
@ObjectType()
export class School {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	@Field()
	name!: string

	@OneToMany(type => EmailSuffix, emailSuffix => emailSuffix.school, { cascade: true, lazy: true })
	@Field(type => [EmailSuffix], { nullable: true })
	emailSuffixes!: Lazy<EmailSuffix[]>

	@OneToMany(type => Campus, campus => campus.school, { cascade: true, lazy: true })
	@Field(type => [Campus], { nullable: true })
	campuses!: Lazy<Campus[]>

	@OneToMany(type => FieldOfStudy, fieldOfStudy => fieldOfStudy.school, { cascade: true, lazy: true })
	@Field(type => [FieldOfStudy], { nullable: true })
	fieldsOfStudies!: Lazy<FieldOfStudy[]>

	@OneToMany(type => User, user => user.school, { lazy: true })
	@Field(type => [User], { nullable: true })
	students!: User[]

	@Column({ nullable: true })
	@Field({ nullable: true })
	deletedAt?: Date

	constructor(name: string|undefined = undefined, emailSuffixes: string[] = [], fieldOfStudies: string[] = []) {
		if (name !== undefined) {
			this.name = name
			this.emailSuffixes = emailSuffixes.map(suffix => new EmailSuffix(suffix))
			this.fieldsOfStudies = fieldOfStudies.map(fieldOfStudy => new FieldOfStudy(fieldOfStudy))
		}
	}
}

@InputType()
export class SchoolInput implements Partial<School> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field({ nullable: true })
	name!: string

	@Field(type => [EmailSuffixInput], { nullable: true })
	emailSuffixes!: EmailSuffix[]

	@Field(type => [CampusInput], { nullable: true })
	campuses!: Campus[]
}
