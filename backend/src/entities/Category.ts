import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Post } from './Post'
import { ObjectType, Field, ID, InputType } from 'type-graphql'

@Entity()
@ObjectType()
export class Category {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	@Field(type => String)
	name!: string

	@OneToMany(type => Post, post => post.category, { lazy: true })
	@Field(type => Post)
	posts!: Post[]

	@Column({ nullable: true })
	@Field({ nullable: true })
	deletedAt?: Date

	constructor(name: string|undefined = undefined) {
		if (name !== undefined) this.name = name
	}
}

@InputType()
export class CategoryInput implements Partial<Category> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field()
	name!: string
}
