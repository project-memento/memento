import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm'
import { School, SchoolInput } from './School'
import { ObjectType, Field, ID, InputType } from 'type-graphql'

@Entity()
@ObjectType()
export class EmailSuffix {
	@PrimaryColumn({ unique: true })
	@Field(type => ID)
	suffix!: string

	@ManyToOne(type => School, school => school.emailSuffixes, { lazy: true })
	@Field(type => School)
	@JoinColumn()
	school!: School

	constructor(suffix: string|undefined = undefined, school: School|undefined = undefined) {
		if (suffix !== undefined) this.suffix = suffix
		if (school !== undefined) this.school = school
	}
}

@InputType()
export class EmailSuffixInput implements Partial<EmailSuffix> {
	@Field()
	suffix!: string

	@Field(type => SchoolInput, { nullable: true })
	school!: School
}
