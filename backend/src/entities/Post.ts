import { Column, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { User, UserInput } from './User'
import { Category, CategoryInput } from './Category'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { Tag } from './Tag'
import { Campus, CampusInput } from './Campus'
import { AssistanceOffer } from './AssistanceOffer'
import { Lazy } from '../helpers/Lazy'

@Entity()
@ObjectType()
export class Post {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	@Field()
	title!: string

	@Column()
	@Field()
	content!: string

	@Column()
	@Field()
	deadline!: Date

	@ManyToOne(type => Campus, { eager: true, cascade: ['insert'] })
	@Field(type => Campus)
	location!: Campus

	@ManyToOne(type => User, user => user.posts, { eager: true })
	@Field(type => User)
	user!: User

	@ManyToOne(type => Category, category => category.posts, { eager: true, cascade: ['insert'] })
	@Field(type => Category)
	category!: Category

	@OneToMany(type => AssistanceOffer, offer => offer.post, { nullable: true, lazy: true })
	@Field(type => [AssistanceOffer])
	assistanceOffers!: Lazy<AssistanceOffer[]>

	@ManyToMany(type => Tag, tag => tag.posts, { eager: true, cascade: ['insert'] })
	@Field(type => Tag)
	tags!: Tag[]

	@Column()
	@Field()
	imageLink!: string

	constructor(
		title: string|undefined = undefined,
		content: string|undefined = undefined,
		deadline: Date|undefined = undefined,
		location: Campus|undefined = undefined,
		user: User|undefined = undefined,
		category: Category|undefined = undefined,
		tags: Tag[] = [],
		imageLink: string|undefined = undefined) {
		if (title !== undefined) {
			this.title = title
			// @ts-ignore
			this.content = content
			// @ts-ignore
			this.deadline = deadline
			// @ts-ignore
			this.location = location
			// @ts-ignore
			this.user = user
			// @ts-ignore
			this.category = category
			this.tags = tags
			// @ts-ignore
			this.imageLink = imageLink
		}
	}
}

@InputType()
export class PostInput implements Partial<Post> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field()
	title!: string

	@Field()
	content!: string

	@Field()
	deadline!: Date

	@Field(type => CampusInput)
	location!: Campus

	@Field()
	imageLink!: string

	@Field(type => UserInput)
	user!: User

	@Field(type => CategoryInput)
	category!: Category
}
