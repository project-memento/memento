import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { User } from './User'
import { Appointment, AppointmentInput } from './Appointment'
import { Post, PostInput } from './Post'

@Entity()
@ObjectType()
export class AssistanceOffer {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column({ nullable: true })
	@Field({ nullable: true })
	description!: string

	@ManyToOne(type => User, { eager: true })
	@Field(type => User)
	helper!: User

	@ManyToOne(type => Post, { lazy: true })
	@Field(type => Post)
	post!: Post

	@OneToOne(type => Appointment, appointment => appointment.assistanceOffer)
	@Field(type => Appointment, { nullable: true })
	appointment!: Appointment

	@Column()
	@Field()
	accepted!: boolean

	constructor(
		helper: User|undefined = undefined,
		post: Post|undefined = undefined,
		description: string|undefined = undefined,
		accepted: boolean|undefined = undefined,
		appointment: Appointment|undefined = undefined) {
		if (helper !== undefined) this.helper = helper
		if (description !== undefined) this.description = description
		if (post !== undefined) this.post = post
		if (appointment !== undefined) this.appointment = appointment
		if (accepted !== undefined) this.accepted = accepted
	}
}

@InputType()
export class AssistanceOfferInput implements Partial<AssistanceOffer> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field({ nullable: true })
	description!: string

	@Field(type => PostInput, { nullable: true })
	post!: Post

	@Field(type => AppointmentInput, { nullable: true })
	appointment!: Appointment

	@Field({ nullable: true })
	accepted!: boolean
}
