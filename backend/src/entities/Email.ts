import { Column, Entity, ManyToOne, PrimaryColumn, RelationId, Unique } from 'typeorm'
import { User, UserInput } from './User'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { IsEmail } from 'class-validator'

@Entity()
@ObjectType()
export class Email {
	@PrimaryColumn({ unique: true })
	@Field()
	@IsEmail()
	email!: string

	@Column()
	@Field()
	isPrimary!: boolean

	@Column({ default: false })
	@Field()
	isVerified!: boolean

	@ManyToOne(type => User, user => user.emails, { cascade: ['insert'] })
	@Field(type => User)
	user!: User

	constructor(email: string|undefined = undefined, isPrimary: boolean = true) {
		if (email !== undefined) {
			this.email = email
			this.isPrimary = true
		}
	}
}

@InputType()
export class EmailInput implements Partial<Email> {
	@Field()
	email!: string

	@Field({ defaultValue: false })
	isPrimary!: boolean

	@Field(type => UserInput, { nullable: true })
	user!: User
}
