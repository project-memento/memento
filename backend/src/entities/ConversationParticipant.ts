import { BeforeInsert, Entity, Index, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, Int } from 'type-graphql'
import { Conversation } from './Conversation'
import { User } from './User'
import { Message } from './Message'

@Entity()
@Index(['conversation', 'user'], { unique: true })
@ObjectType()
export class ConversationParticipant {
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@ManyToOne(type => Conversation, conversation => conversation.participants, { lazy: true })
	@Field(type => Conversation)
	conversation!: Conversation

	@ManyToOne(type => User, user => user.conversations, { eager: true })
	@Field(type => User)
	user!: User

	@ManyToOne(type => Message, { nullable: true, eager: true })
	@Field(type => Message, { nullable: true })
	lastReadMessage!: Message|null

	@Field(type => Int)
	unreadMessages!: number

	constructor(conversation: Conversation|undefined = undefined, user: User|undefined = undefined) {
		// if (conversation) this.conversation = conversation
		if (user) this.user = user
	}
}
