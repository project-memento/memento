import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn, ManyToMany, JoinTable, JoinColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { Campus, CampusInput } from './Campus'
import { User } from './User'
import { AssistanceOffer, AssistanceOfferInput } from './AssistanceOffer'

@Entity()
@ObjectType()
export class Appointment {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@ManyToMany(type => User, user => user.appointments, { eager: true })
	@Field(type => [User])
	@JoinTable()
	users!: User[]

	@Column()
	@Field()
	date!: Date

	@OneToOne(type => AssistanceOffer, assistanceOffer => assistanceOffer.appointment, { eager: true })
	@Field(type => AssistanceOffer)
	@JoinColumn()
	assistanceOffer!: AssistanceOffer

	@ManyToOne(type => Campus, { eager: true })
	@Field(type => Campus)
	location!: Campus

	constructor(
		date: Date|undefined = undefined,
		users: User[]|undefined = undefined,
		location: Campus|undefined = undefined,
		assistanceOffer: AssistanceOffer|undefined = undefined) {
		if (date !== undefined) this.date = date
		if (assistanceOffer !== undefined) this.assistanceOffer = assistanceOffer
		if (location !== undefined) this.location = location
		if (users !== undefined) this.users = users
	}
}

@InputType()
export class AppointmentInput implements Partial<Appointment> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field({ nullable: true })
	date!: Date

	@Field(type => AssistanceOfferInput, { nullable: true })
	assistanceOffer!: AssistanceOffer

	@Field(type => CampusInput, { nullable: true })
	location!: Campus
}
