import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { User } from './User'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { StudentYear } from './StudentYear'
import { Enum } from '../helpers/Enum'

@Entity()
@ObjectType()
export class UserProfile {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	@Field()
	firstname!: string

	@Column()
	@Field()
	lastname!: string

	@Column({ nullable: true })
	@Field({ nullable: true })
	imageLink!: string

	@OneToOne(type => User, user => user.profile)
	@Field(type => User)
	@JoinColumn()
	user!: User

	@Enum(StudentYear, { nullable: true })
	@Field(type => StudentYear, { nullable: true })
	currentYear!: StudentYear
}

@InputType()
export class UserProfileInput implements Partial<UserProfile> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field({ nullable: true })
	firstname!: string

	@Field({ nullable: true })
	lastname!: string

	@Field({ nullable: true })
	imageLink?: string

	@Field(type => StudentYear, { nullable: true })
	currentYear?: StudentYear
}
