import { Column, Entity, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { UserProfile, UserProfileInput } from './UserProfile'
import { Post } from './Post'
import { Tag } from './Tag'
import { Email, EmailInput } from './Email'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { Lazy } from '../helpers/Lazy'
import { Campus, CampusInput } from './Campus'
import { School, SchoolInput } from './School'
import { FieldOfStudy, FieldOfStudyInput } from './FieldOfStudy'
import { Appointment } from './Appointment'
import { Conversation } from './Conversation'
import { Role } from './Role'
import { Enum } from '../helpers/Enum'
import { ConversationParticipant } from './ConversationParticipant'

@Entity()
@ObjectType()
export class User {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	password!: string

	@OneToOne(type => UserProfile, profile => profile.user, { cascade: true, eager: true })
	@Field(type => UserProfile)
	profile!: UserProfile

	@OneToMany(type => Post, post => post.user, { lazy: true })
	@Field(type => Post)
	posts!: Post[]

	@ManyToMany(type => Tag, tag => tag.users, { eager: true })
	@Field(type => [Tag])
	skills!: Tag[]

	@OneToMany(type => Email, email => email.user, { eager: true, cascade: ['insert', 'remove'] })
	@Field(type => [Email])
	emails!: Lazy<Email>[]

	@ManyToMany(type => Campus, campus => campus.students, { lazy: true })
	@Field(type => [Campus])
	campuses!: Campus[]

	@ManyToOne(type => School, school => school.students, { eager: true, cascade: ['insert', 'remove'], nullable: true })
	@Field(type => School, { nullable: true })
	school!: School|string

	@ManyToOne(type => FieldOfStudy, fieldOfStudy => fieldOfStudy.students, { eager: true, nullable: true })
	@Field(type => FieldOfStudy, { nullable: true })
	fieldOfStudy!: FieldOfStudy

	@ManyToMany(type => Appointment, appointment => appointment.users)
	@Field(type => [Appointment])
	appointments!: Appointment[]

	@Enum(Role)
	@Field(type => Role)
	role!: Role

	@OneToMany(type => ConversationParticipant, conversation => conversation.user, { lazy: true })
	@Field(type => [ConversationParticipant])
	conversations!: Lazy<ConversationParticipant[]>

	constructor(password: string|undefined = undefined, profile: UserProfile|undefined = undefined) {
		if (password !== undefined) {
			this.password = password
			if (profile !== undefined) {
				this.profile = profile
			}
		}
	}
}

@InputType()
export class UserInput implements Partial<User> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field({ nullable: true })
	password!: string

	@Field(type => UserProfileInput, { nullable: true })
	profile!: UserProfile

	@Field(type => [EmailInput], { nullable: true })
	emails!: Email[]

	@Field(type => SchoolInput, { nullable: true })
	school?: School

	@Field(type => [CampusInput], { nullable: true })
	campuses?: Campus[]

	@Field(type => FieldOfStudyInput, { nullable: true })
	fieldOfStudy?: FieldOfStudy
}
