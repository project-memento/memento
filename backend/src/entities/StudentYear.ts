import { registerEnumType } from 'type-graphql'

export enum StudentYear {
	FIRST_YEAR,
	SECOND_YEAR,
	THIRD_YEAR,
	FIRST_MASTER,
	SECOND_MASTER
}

registerEnumType(StudentYear, {
	name: 'StudentYear',
	description: 'The year a student is on. E.g. "1. Studieår"'
})
