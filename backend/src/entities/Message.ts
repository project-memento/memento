import { BeforeInsert, Column, CreateDateColumn, Entity, ManyToOne, PrimaryColumn } from 'typeorm'
import { Field, ID, ObjectType } from 'type-graphql'
import { Conversation } from './Conversation'
import { User } from './User'
import { monotonicFactory, factory } from 'ulid'

export const monotonic = factory()

@Entity()
@ObjectType()
export class Message {
	@PrimaryColumn({ type: 'varchar', length: 26 })
	@Field(type => ID)
	id!: string

	@ManyToOne(type => Conversation, conversation => conversation.messages)
	@Field(type => Conversation)
	conversation!: Conversation

	@Column()
	@Field()
	message!: string

	@ManyToOne(type => User, { lazy: true })
	@Field(type => User)
	author!: User

	@Column()
	@Field(type => Date)
	sentAt!: Date

	constructor(conversation: Conversation|string|undefined = undefined,
		message: string = '',
		author: User|string|undefined = undefined,
		sentAt: Date|undefined = undefined
	) {
		if (typeof conversation !== 'undefined') {
			this.conversation = conversation as Conversation
			this.message = message
			this.author = author as User
			if (author === undefined) {
				throw new Error('Author cannot be undefined')
			}
		}
		if (sentAt !== undefined) {
			this.id = monotonic(sentAt.getTime())
			this.sentAt = sentAt
		} else {
			this.sentAt = new Date(Date.now())
		}
	}

	@BeforeInsert()
	beforeInsert() {
		if (!this.id) {
			this.id = monotonic()
		}
	}
}
