import { registerEnumType } from 'type-graphql'

export enum Role {
	USER,
	ADMIN
}

registerEnumType(Role, {
	name: 'Role',
	description: 'User or admin atm.'
})
