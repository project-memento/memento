import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { User } from './User'
import { School, SchoolInput } from './School'
import { Lazy } from '../helpers/Lazy'

@Entity()
@ObjectType()
export class FieldOfStudy {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	@Field()
	name!: string

	@OneToMany(type => User, user => user.fieldOfStudy, { lazy: true })
	students!: Lazy<User>[]

	@ManyToOne(type => School, school => school.fieldsOfStudies)
	@Field(type => School)
	school!: School|string

	@Column({ nullable: true })
	@Field({ nullable: true })
	deletedAt?: Date

	constructor(name: string|undefined = undefined, school: School|undefined = undefined) {
		if (name !== undefined) {
			this.name = name
			if (school !== undefined) this.school = school
		}
	}
}

@InputType()
export class FieldOfStudyInput implements Partial<FieldOfStudy> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field()
	name!: string

	@Field(type => SchoolInput, { nullable: true })
	school!: string

	@Field({ nullable: true })
	deletedAt!: Date
}
