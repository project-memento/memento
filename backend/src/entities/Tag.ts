import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm'
import { User } from './User'
import { Post } from './Post'
import { ObjectType, Field, ID, InputType } from 'type-graphql'

@Entity()
@ObjectType()
export class Tag {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@Column()
	@Field()
	tag!: string

	@ManyToMany(type => User, user => user.skills)
	@JoinTable()
	@Field(type => User)
	users!: User[]

	@ManyToMany(type => Post, post => post.tags, { lazy: true })
	@JoinTable()
	@Field(type => Post)
	posts!: Post[]

	@Column({ nullable: true })
	@Field({ nullable: true })
	deletedAt?: Date

	constructor(tag: string|undefined = undefined) {
		if (tag !== undefined) {
			this.tag = tag
		}
	}
}

@InputType()
export class TagInput implements Partial<Tag> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field()
	tag!: string
}
