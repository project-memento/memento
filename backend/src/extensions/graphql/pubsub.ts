// @ts-ignore
import { PostgresPubSub } from 'graphql-postgres-subscriptions'
import { Client } from 'pg'
import { host, database, type, username, password, port } from '../../TypeOrm'

export const getConfiguredRedisPubSub = async () => {
	if (type === 'postgres') {
		const client = new Client({
			database,
			host,
			user: username,
			password,
			port
		})
		await client.connect()
		return new PostgresPubSub({ client })
	}
}
