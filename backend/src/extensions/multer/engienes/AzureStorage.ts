import { StorageEngine } from 'multer'
import {
	Aborter,
	BlobURL,
	BlockBlobURL,
	ContainerURL, Credential,
	ServiceURL,
	SharedKeyCredential,
	StorageURL,
	uploadStreamToBlockBlob
} from '@azure/storage-blob'
import { Readable } from 'stream'
import { BlobType } from '@azure/storage-blob/lib/generated/lib/models/index'

export type ASResolver<T> = (req: Express.Request, file: Express.Multer.File) => Promise<T>

export type Metadata = { [k: string]: string }

export interface MulterInFile extends Express.Multer.File {
	stream: Readable
}

export interface MulterOutFile extends Express.Multer.File {
	azure: {
		url: string,
		container: string,
		blobName: string,
		eTag?: string,
		blobType?: BlobType,
		metadata?: Metadata,
		blobSize?: number,
	}
}

export interface AzureStorageOptions {
	credential: Credential,
	accessKey: string,
	accountName: string,
	containerName: string,
	blobName: ASResolver<string>,
	metadata: ASResolver<Metadata>|Metadata,
}

export class AzureStorage implements StorageEngine {
	private readonly _blobName: (req: Express.Request, file: Express.Multer.File) => Promise<string>
	private readonly _serviceURL: ServiceURL
	private readonly _containerURL: ContainerURL
	private _resolveMetadata: ASResolver<Metadata>

	constructor(private options: AzureStorageOptions) {
		this._blobName = options.blobName

		this._resolveMetadata = this._wrapInASResolver(options.metadata)

		const pipeline = StorageURL.newPipeline(options.credential)

		this._serviceURL = new ServiceURL(
			`https://${options.accountName}.blob.core.windows.net`,
			pipeline
		)

		this._containerURL = ContainerURL.fromServiceURL(this._serviceURL, this.options.containerName)
	}

	async _handleFile(req: Express.Request, file: MulterInFile, callback: (error?: any, info?: Partial<Express.Multer.File>) => void): Promise<void> {
		const blobName = await this._blobName(req, file)
		try {
			const blobURL = BlobURL.fromContainerURL(this._containerURL, blobName)

			const blockBlobURL = BlockBlobURL.fromBlobURL(blobURL)
			await uploadStreamToBlockBlob(
				Aborter.timeout(2 * 60 * 60 * 1000),
				file.stream,
				blockBlobURL,
				file.stream.readableLength,
				20,
				{
					blobHTTPHeaders: {
						blobContentType: file.mimetype,
						blobContentDisposition: 'inline'
					},
					metadata: await this._resolveMetadata(req, file)
				}
			)

			const url = blobURL.url
			const blobProperties = await blockBlobURL.getProperties(Aborter.none)
			const intermediateFile: Partial<MulterOutFile> = {
				filename: blobName,
				azure: {
					url,
					container: this.options.containerName,
					blobName,
					eTag: blobProperties.eTag,
					blobType: blobProperties.blobType,
					metadata: blobProperties.metadata,
					blobSize: blobProperties.contentLength
				},
			}
			const finalFile = {
				...file,
				...intermediateFile
			}
			callback(null, finalFile)
		} catch (e) {
			callback(e)
		}
	}

	async _removeFile(req: Express.Request, file: MulterOutFile, callback: (error: Error) => void): Promise<void> {
		try {
			const blobURL = BlobURL.fromContainerURL(this._containerURL, file.azure.blobName)
			await blobURL.delete(
				Aborter.none,
			)
		} catch (e) {
			callback(e)
		}
	}

	private _wrapInASResolver<T>(value: (ASResolver<T>|T|Promise<T>)): ASResolver<T> {
		return (req, file) => {
			if (typeof value === 'function') {
				// @ts-ignore
				return Promise.resolve(value(req, file))
			} else {
				return Promise.resolve(value)
			}
		}
	}
}
