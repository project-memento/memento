import { v4 } from 'uuid'
import { existsSync, mkdirSync } from 'fs'
import { diskStorage } from 'multer'
import { ASResolver, AzureStorage, Metadata } from './engienes/AzureStorage'
import { SharedKeyCredential } from '@azure/storage-blob'
import { extname } from 'path'

const resolveFilename: ASResolver<string> = (req: any, file: Express.Multer.File): Promise<string> => {
	return Promise.resolve(`${Date.now()}-${v4()}${extname(file.originalname)}`)
}

const resolveMetadata: ASResolver<Metadata> = (req: any, file: Express.Multer.File): Promise<Metadata> => {
	return Promise.resolve({
		userId: req.user.id
	})
}

export const DiskStorage = (publicDir : string) => diskStorage({
	destination: function (req, file, cb) {
		if (!existsSync(`${publicDir}uploads/${req.user.id}`)) {
			mkdirSync(`${publicDir}uploads/${req.user.id}`, { recursive: true })
		}
		cb(null, `/${publicDir}uploads/${req.user.id}`)
	},
	filename: async function (req, file, cb) {
		const filename = await resolveFilename(req, file)
		cb(null, filename)
	}
})

export const azureStorage = ({ accountKey, accountName, containerName }: any) => new AzureStorage({
	credential: new SharedKeyCredential(accountName, accountKey),
	accessKey: accountKey,
	accountName,
	containerName,
	metadata: resolveMetadata,
	blobName: resolveFilename
})
