import { Controller, Post, Req } from '@tsed/common'
import { MultipartFile } from '@tsed/multipartfiles'
import { MulterOutFile } from '../../extensions/multer/engienes/AzureStorage'

@Controller('/api/v1')
export class UploadController {
	@Post('/upload')
	private async uploadFile(@MultipartFile('file') file: MulterOutFile, @Req() request: Express.Request) {
		console.log({ file })
		if (file.azure !== undefined) {
			return {
				path: file.azure.url
			}
		}
		return {
			path: `/uploads/${request.user.id}/${file.filename}`
		}
	}
}
