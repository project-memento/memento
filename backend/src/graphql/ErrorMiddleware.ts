import { $log } from 'ts-log-debug'
import { MiddlewareFn } from 'type-graphql'

export const ErrorInterceptor: MiddlewareFn<any> = async ({ context, info }, next) => {
	try {
		return await next()
	} catch (err) {
		// write error to file log
		$log.error(err, context, info)

		// hide errors from db like printing sql query
		throw new Error('Internal server error')
	}
}
