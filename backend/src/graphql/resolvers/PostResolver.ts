import { ResolverService } from '@tsed/graphql'
import { Post } from '../../entities/Post'
import { Arg, Authorized, Ctx, Mutation, Query, Subscription, PubSub, Publisher, FieldResolver, Root } from 'type-graphql'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { In, LessThan, LessThanOrEqual, Not, Repository } from 'typeorm'
import { Tag } from '../../entities/Tag'
import { User } from '../../entities/User'
import { Category } from '../../entities/Category'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { Forbidden } from 'ts-httpexceptions'
import { Campus } from '../../entities/Campus'
import { AzureBlobService } from '../../services/AzureBlobService'
import { basename } from 'path'
import { AssistanceOffer } from '../../entities/AssistanceOffer'

@ResolverService(Post)
export class PostResolver implements AfterDBInit {
	tagRepo!: Repository<Tag>
	postRepo!: Repository<Post>
	userRepo!: Repository<User>
	categoryRepo!: Repository<Category>
	campusRepo!: Repository<Campus>
	assistanceOfferRepo!: Repository<AssistanceOffer>

	$afterDBInit() {
		this.tagRepo = this.repoService.getRepository(Tag)
		this.postRepo = this.repoService.getRepository(Post)
		this.userRepo = this.repoService.getRepository(User)
		this.categoryRepo = this.repoService.getRepository(Category)
		this.campusRepo = this.repoService.getRepository(Campus)
		this.assistanceOfferRepo = this.repoService.getRepository(AssistanceOffer)
	}

	constructor(private repoService: RepositoryService, private azureBlobService: AzureBlobService) {
	}

	@Query(type => Post)
	post(@Arg('postId') postId: string): Promise<Post|undefined> {
		const post = this.postRepo.findOne(postId)
		if (post === undefined) {
			throw new EntityNotFoundError(Post, postId)
		}

		return post
	}

	@Query(type => [Post])
	async posts(
		@Arg('categoryIds', type => [String], { defaultValue: [] }) categoryIds: string[],
		@Arg('campusIds', type => [String], { defaultValue: [] }) campusIds: string[],
		@Arg('tagIds', type => [String], { defaultValue: [] }) tagIds: string[],
		@Arg('me', type => Boolean, { defaultValue: false }) me: boolean,
		@Arg('active', type => Boolean, { defaultValue: true }) active: boolean,
		@Arg('order', type => String, { defaultValue: null, nullable: true }) order: 'ASC'|'DESC'|null,
		@Ctx() ctx: any
	): Promise<Post[]> {
		let conditions: any = {}
		if (me) {
			conditions['user'] = { id: ctx.user.id }
		} else {
			conditions['user'] = { id: Not(ctx.user.id) }
		}
		if (campusIds.length > 0) conditions['location'] = { id: In(campusIds) }
		if (categoryIds.length > 0) conditions['category'] = { id: In(categoryIds) }
		conditions['deadline'] = active
			? Not(LessThanOrEqual(new Date()))
			: LessThan(new Date())
		if (tagIds.length > 0) {
			const ids = await this.postRepo
				.createQueryBuilder('posts')
				.leftJoinAndSelect('posts.tags', 'tags')
				.where('tags.id IN(:...ids)')
				.setParameter('ids', tagIds)
				.select('posts.id')
				.getMany()
			return this.postRepo.findByIds(ids.map(tag => tag.id), {
				where: conditions,
				order: order !== null ? {
					title: order
				} : {
					deadline: 'ASC'
				}
			})
		} else {
			return this.postRepo.find({
				where: conditions,
				order: order !== null ? {
					title: order
				} : {
					deadline: 'ASC'
				}
			})
		}
	}

	@Subscription(type => [Post], { topics: 'POSTS', name: 'posts' })
	async subscribePosts(
		@Arg('categoryIds', type => [String], { nullable: true }) categoryIds: string[],
		@Arg('skillTags', type => [String], { nullable: true }) skillTags: string[]
	): Promise<Post[]> {
		let conditions: any = {}
		if (categoryIds !== undefined && categoryIds.length > 0) conditions['category'] = { id: In(categoryIds) }
		// if (skillTags !== undefined) conditions['tags'] = { id: In(skillTags) }
		return this.postRepo.find(conditions)
	}

	@Authorized()
	@Mutation(type => Post)
	async createPost(
		@Arg('title') title: string,
		@Arg('message') content: string,
		@Arg('deadline') deadline: Date,
		@Arg('location') location: string,
		@Arg('imageLink') imageLink: string,
		@Arg('categoryId') categoryId: string,
		@Arg('tagIds', type => [String]) tagIds: string[],
		@PubSub('POSTS') publish: Publisher<any>,
		@Ctx() ctx: any): Promise<Post> {
		const post = new Post(
			title,
			content,
			deadline,
			await this.campusRepo.findOne(location),
			ctx.user.id,
			await this.categoryRepo.findOne(categoryId),
			await this.tagRepo.findByIds(tagIds),
			imageLink)
		const result = this.postRepo.save(post)
		await publish(null)
		return result
	}

	@Authorized()
	@Mutation(type => Post)
	async updatePost(
		@Arg('title') title: string,
		@Arg('message') content: string,
		@Arg('deadline') deadline: Date,
		@Arg('location') location: string,
		@Arg('categoryId') categoryId: string,
		@Arg('postId') postId: string,
		@Arg('tagIds', type => [String]) tagIds: string[],
		@Ctx() ctx: any): Promise<Post> {
		const post = await this.postRepo.findOne(postId)
		if (post === undefined) throw new EntityNotFoundError(Post, postId)
		if (post.user.id !== ctx.user.id) {
			throw new Forbidden('You are not authorized to edit this post')
		}
		post.title = title
		post.content = content
		post.category = await this.categoryRepo.findOne(categoryId) as Category
		post.location = await this.campusRepo.findOne(location) as Campus
		post.deadline = deadline
		post.tags = await this.tagRepo.findByIds(tagIds)

		return this.postRepo.save(post)
	}

	@Authorized()
	@Mutation(type => Post)
	async deletePost(@Arg('postId') postId: string, @Ctx() ctx: any) {
		const post = await this.postRepo.findOne(postId)
		if (post === undefined) throw new EntityNotFoundError(Post, postId)
		if (postId !== ctx.user.id) {
			throw new Forbidden('You are not authorized to delete this post')
		}

		await this.postRepo.delete(post)
	}

	@FieldResolver()
	async assistanceOffers(@Root() post: Post, @Ctx() ctx: any) {
		const offers = await this.assistanceOfferRepo.find({ post })
		if (post.user.id === ctx.user.id) return offers
		const offer = offers.find(offer => offer.helper.id === ctx.user.id)
		if (offer) {
			return [offer]
		}
		return []
	}

	@FieldResolver()
	async imageLink(@Root() post: Post) {
		const raw = post.imageLink
		if (/^https/.test(raw)) {
			return `${raw}?${this.azureBlobService.generateSharedAccessSignature(basename(raw))}`
		}
		return raw
	}
}
