import { ResolverService } from '@tsed/graphql'
import { Message, monotonic } from '../../entities/Message'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { LessThan, Repository } from 'typeorm'
import { Arg, Ctx, Mutation, Publisher, PubSub, Query, Subscription, Root, FieldResolver, ResolverFilterData } from 'type-graphql'
import PaginatedResponse from '../types/PaginatedResponse'
import { Conversation } from '../../entities/Conversation'
import { inputMapper } from '../../helpers/InputMapper'
import { PUB_SUB_MESSAGES } from '../Constants'
import { User } from '../../entities/User'
import { PaginatedMessageResponse } from '../types/PaginatedMessageResponse'
import { ConversationParticipant } from '../../entities/ConversationParticipant'

@ResolverService(Message)
export class MessageResolver implements AfterDBInit {
	private conversationParticipantRepo!: Repository<ConversationParticipant>
	private messageRepo!: Repository<Message>
	private conversationRepo!: Repository<Conversation>
	private userRepo!: Repository<User>

	constructor(private repoService: RepositoryService) {
	}

	$afterDBInit() {
		this.conversationParticipantRepo = this.repoService.getRepository(ConversationParticipant)
		this.messageRepo = this.repoService.getRepository(Message)
		this.conversationRepo = this.repoService.getRepository(Conversation)
		this.userRepo = this.repoService.getRepository(User)
	}

	@Query(type => PaginatedMessageResponse, { name: 'messages' })
	async messages(@Ctx() ctx: any,
		@Arg('conversationId') conversationId: string,
		@Arg('after', type => String, { defaultValue: null, nullable: true }) after: string|null
	): Promise<PaginatedMessageResponse> {
		const conversation = await this.conversationRepo.findOne(conversationId)
		if (typeof conversation === 'undefined' || !conversation.participants.some(p => p.user.id === ctx.user.id)) {
			throw new Error('Unknown conversationId')
		}
		const [items, left] = await this.messageRepo.findAndCount({
			where: {
				conversation: { id: conversationId },
				...(after ? { id: LessThan(after) } : {})
			},
			order: {
				id: 'DESC'
			},
			take: 25
		})
		const total = await this.messageRepo.count({
			conversation: { id: conversationId }
		})
		return {
			items,
			total,
			hasMore: (left - 25) > 0
		}
	}

	@Subscription(type => Message, {
		name: 'messages',
		topics: PUB_SUB_MESSAGES,
		filter: ({ payload, context }: ResolverFilterData<Message, any, any>) =>
			payload.conversation.participants.map(u => u.user.id).includes(context.user.id)
	})
	async subscribeToMessages(@Ctx() ctx: any, @Root() message: Message) {
		/*
		const conversation = await this.conversationRepo
		if (typeof conversation === 'undefined' || !conversation.participants.some(p => p.id === ctx.user.id)) {
			throw new Error('Unknown conversationId')
		}
		*/
		return message
	}

	@Mutation(type => Message)
	async sendMessage(@Ctx() ctx: any,
		@Arg('conversationId') conversationId: string,
		@Arg('message') message: string,
		@PubSub(PUB_SUB_MESSAGES) publisher: Publisher<Message>
	): Promise<Message> {
		const conversation = await this.conversationRepo.findOne(conversationId)
		if (typeof conversation === 'undefined' || !conversation.participants.some(p => p.user.id === ctx.user.id)) {
			throw new Error('Unknown conversationId')
		}
		const msg = await this.messageRepo.save(inputMapper(
			{
				id: monotonic(),
				conversation,
				message,
				author: ctx.user.id
			},
			Message
		))
		await this.conversationParticipantRepo.update({
			conversation: conversation,
			user: ctx.user
		}, {
			lastReadMessage: msg
		})

		await publisher(msg)
		return msg
	}

	@FieldResolver()
	async author(@Root() message: Message) {
		const author = await Promise.resolve(message.author)
		const id = typeof author === 'string' ? author : author.id
		return this.userRepo.findOne(id)
	}
}
