import { ResolverService } from '@tsed/graphql'
import { Query, Arg, Ctx } from 'type-graphql'
import { UserService } from '../../services/UserService'
import { Email } from '../../entities/Email'
import { RepositoryService } from '../../services/RepositoryService'

@ResolverService(String)
export class AuthenticationResolver {
	constructor(private userService: UserService, private repoService: RepositoryService) {
	}

	@Query(type => String, { description: 'Returns JWT token' })
	async login(@Arg('email') email: string, @Arg('password') password: string, @Ctx() ctx: any) {
		return this.userService.login(email, password)
	}
}
