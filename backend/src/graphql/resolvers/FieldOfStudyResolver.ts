import { ResolverService } from '@tsed/graphql'
import { FieldOfStudy, FieldOfStudyInput } from '../../entities/FieldOfStudy'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { Repository } from 'typeorm'
import { Query, Arg, Mutation, Authorized, FieldResolver, Root } from 'type-graphql'
import { inputMapper } from '../../helpers/InputMapper'
import { School } from '../../entities/School'
import { User } from '../../entities/User'

@ResolverService(FieldOfStudy)
export class FieldOfStudyResolver implements AfterDBInit {
	private fieldOfStudyRepo!: Repository<FieldOfStudy>
	constructor(private repoService: RepositoryService) {

	}

	$afterDBInit(): void | Promise<any> {
		this.fieldOfStudyRepo = this.repoService.getRepository(FieldOfStudy)
	}

	@Query(type => [FieldOfStudy])
	async fieldsOfStudies(@Arg('schoolId') schoolId: string) {
		return this.fieldOfStudyRepo.find({ school: { id: schoolId } })
	}

	@Mutation(type => FieldOfStudy)
	async createFieldOfStudy(@Arg('fieldOfStudyInput') fieldOfStudyInput: FieldOfStudyInput) {
		const fieldOfStudy = inputMapper(fieldOfStudyInput, FieldOfStudy)
		fieldOfStudy.school = inputMapper(fieldOfStudyInput.school as any, School)
		return this.fieldOfStudyRepo.save(fieldOfStudy)
	}

	@Authorized()
	@FieldResolver(type => [User])
	async students(@Root() fieldOfStudy: FieldOfStudy) {
		return fieldOfStudy.students
	}
}
