import { ResolverService } from '@tsed/graphql'
import { ConversationParticipant } from '../../entities/ConversationParticipant'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { LessThan, Repository, MoreThan } from 'typeorm'
import { Message } from '../../entities/Message'
import { FieldResolver, Root, Int } from 'type-graphql'

@ResolverService(type => ConversationParticipant)
export class ConversationParticipantResolver implements AfterDBInit {
	private messageRepo!: Repository<Message>

	constructor(private repoService: RepositoryService) {}

	$afterDBInit() {
		this.messageRepo = this.repoService.getRepository(Message)
	}

	@FieldResolver(type => Int)
	async unreadMessages(@Root() participant: ConversationParticipant) {
		return this.messageRepo.count({
			where: {
				conversation: await participant.conversation,
				...(participant.lastReadMessage
					? { id: MoreThan(participant.lastReadMessage.id) }
					: {})
			},
			order: {
				id: 'DESC'
			}
		})
	}
}
