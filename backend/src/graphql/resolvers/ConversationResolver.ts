import { ResolverService } from '@tsed/graphql'
import { Conversation } from '../../entities/Conversation'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { In, Repository } from 'typeorm'
import { Query, Authorized, Ctx, FieldResolver, Root, Mutation, Arg } from 'type-graphql'
import { User } from '../../entities/User'
import { Message } from '../../entities/Message'
import { ConversationParticipant } from '../../entities/ConversationParticipant'

@ResolverService(type => Conversation)
export class ConversationResolver implements AfterDBInit {
	private conversationParticipantRepo!: Repository<ConversationParticipant>
	private conversationRepo!: Repository<Conversation>
	private userRepo!: Repository<User>
	private messageRepo!: Repository<Message>
	constructor(private repoService: RepositoryService) {}

	$afterDBInit() {
		this.conversationParticipantRepo = this.repoService.getRepository(ConversationParticipant)
		this.conversationRepo = this.repoService.getRepository(Conversation)
		this.userRepo = this.repoService.getRepository(User)
		this.messageRepo = this.repoService.getRepository(Message)
	}

	@Authorized()
	@Query(type => [Conversation])
	async conversations(@Ctx() ctx: any) {
		const conversations = await this.conversationParticipantRepo.find({
			where: {
				user: ctx.user.id
			},
			relations: [
				'conversation'
			]
		})

		return Promise.all(conversations.map(relation => relation.conversation))
	}

	@Authorized()
	@Mutation(type => Message)
	async readMessage(@Arg('messageId') messageId: string, @Ctx() ctx: any) {
		const message = await this.messageRepo.findOne(messageId, {
			relations: [
				'conversation',
				'conversation.participants'
			]
		})
		if (message) {
			const participant = message.conversation.participants
				.find(({ user }) => user.id === ctx.user.id) as ConversationParticipant

			// @ts-ignore
			participant.lastReadMessage = messageId
			return this.conversationParticipantRepo.save(participant)
		}
	}

	@FieldResolver()
	async lastMessage(@Root() conversation: Conversation) {
		return this.messageRepo.findOne({
			where: {
				conversation
			},
			order: {
				id: 'DESC'
			}
		})
	}
}
