import { ResolverService } from '@tsed/graphql'
import { Arg, Ctx, FieldResolver, Mutation, Query, Root } from 'type-graphql'
import { User } from '../../entities/User'
import { UserService } from '../../services/UserService'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { EmailService } from '../../services/EmailService'
import { BadRequest, Forbidden } from 'ts-httpexceptions'
import { EmailSuffixService } from '../../services/EmailSuffixService'
import { inputMapper } from '../../helpers/InputMapper'
import { UserProfile, UserProfileInput } from '../../entities/UserProfile'
import { Email } from '../../entities/Email'
import { RepositoryService } from '../../services/RepositoryService'
import { EmailSuffix } from '../../entities/EmailSuffix'
import { Equal, Repository } from 'typeorm'
import { Context } from '@tsed/common'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { Campus } from '../../entities/Campus'
import { School } from '../../entities/School'
import { Tag } from '../../entities/Tag'
import { FieldOfStudy } from '../../entities/FieldOfStudy'
import { Role } from '../../entities/Role'

@ResolverService(User)
export class UserResolver implements AfterDBInit {
	private userRepo!: Repository<User>
	private schoolRepo!: Repository<School>
	constructor(
		private userService: UserService,
		private emailService: EmailService,
		private emailSuffixService: EmailSuffixService,
		private repoService: RepositoryService) {

	}

	$afterDBInit() {
		this.userRepo = this.repoService.getRepository(User)
		this.schoolRepo = this.repoService.getRepository(School)
	}

	@Query(type => User, { nullable: true })
	async me(@Ctx() ctx: Context) {
		if (ctx.user) {
			ctx.user = await this.userRepo.findOne(ctx.user)
			return ctx.user
		}
		return null
	}

	@Query(type => User)
	user(@Arg('userId') userId: string) {
		const user = this.userService.find(userId)
		if (user === undefined) {
			throw new EntityNotFoundError(User, userId)
		}

		return user
	}

	@Mutation(type => User)
	async createUser(
		@Arg('password') password: string,
		@Arg('email') email: string,
		@Arg('profile') profile: UserProfileInput,
		@Arg('schoolId') schoolId: string,
		@Arg('fieldOfStudyId') fieldOfStudyId: string,
		@Arg('skills', type => [String]) skills: string[]
	) {
		email = email.toLowerCase()
		const emailRepo = this.repoService.getRepository(Email)
		if (await emailRepo.count({ email: Equal(email) }) > 0) {
			throw new BadRequest('Email already in use')
		}
		const suffixRepo = this.repoService.getRepository(EmailSuffix)
		if (await suffixRepo.count({ suffix: Equal(email.split('@')[1]) }) === 0) {
			throw new BadRequest('Not a valid email-suffix')
		}

		const user = new User()
		user.password = password
		user.emails = [new Email(email)]
		user.school = schoolId
		user.role = Role.USER
		user.fieldOfStudy = inputMapper({ id: fieldOfStudyId }, FieldOfStudy)
		user.skills = skills.map(id => inputMapper({ id }, Tag))
		user.profile = inputMapper(profile, UserProfile)
		user.profile.imageLink = '/uploads/default.png'
		return this.userService.create(user)
	}

	@Mutation(type => User)
	async updateUser(
		@Arg('profile', { nullable: true }) profile: UserProfileInput,
		@Arg('schoolId', { nullable: true }) schoolId: string,
		@Arg('fieldOfStudyId', { nullable: true }) fieldOfStudyId: string,
		@Arg('skillIds', type => [String], { nullable: true }) skills: string[],
		@Arg('campusIds', type => [String], { nullable: true }) campuses: string[],
		@Ctx() ctx: any
	) {
		if (ctx.user === null) throw new Forbidden('Please sign in')
		const user = ctx.user
		if (schoolId) user.school = schoolId
		if (fieldOfStudyId) user.fieldOfStudy = fieldOfStudyId
		if (skills) user.skills = skills.map(id => inputMapper({ id }, Tag))
		if (profile) user.profile = inputMapper({ ...user.profile, ...profile }, UserProfile)
		if (campuses) user.campuses = campuses.map(campus => inputMapper({ id: campus }, Campus))
		return this.userRepo.save(user)
	}

	@FieldResolver()
	async emails(@Root() user: User, @Ctx() ctx: any) {
		if (user.id !== ctx.user.id) {
			return []
		}
		return user.emails
	}

	@FieldResolver()
	async profile(@Root() user: User) {
		if (!user.profile) {
			// @ts-ignore
			return this.userRepo.findOne(user).then(user => user.profile)
		}
		return user.profile
	}

	@FieldResolver()
	async posts(@Root() user: User) {
		return user.posts
	}

	@FieldResolver()
	async skills(@Root() user: User) {
		return user.skills
	}

	@FieldResolver()
	async school(@Root() user: User) {
		return user.school
	}
}
