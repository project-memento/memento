import { ResolverService } from '@tsed/graphql'
import { UserProfile } from '../../entities/UserProfile'
import { AzureBlobService } from '../../services/AzureBlobService'
import { FieldResolver, Root } from 'type-graphql'
import { basename } from 'path'

@ResolverService(UserProfile)
export class UserProfileResolver {
	constructor(private azureBlobService: AzureBlobService) {}

	@FieldResolver()
	async imageLink(@Root() profile: UserProfile) {
		const raw = profile.imageLink
		if (/^https/.test(raw)) {
			return `${raw}?${this.azureBlobService.generateSharedAccessSignature(basename(raw))}`
		}
		return raw
	}
}
