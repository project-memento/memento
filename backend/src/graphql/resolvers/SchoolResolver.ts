import { ResolverService } from '@tsed/graphql'
import { Query, Mutation, Arg, Subscription, Root, FieldResolver, Authorized } from 'type-graphql'
import { inputMapper } from '../../helpers/InputMapper'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { School } from '../../entities/School'
import { EmailSuffix, EmailSuffixInput } from '../../entities/EmailSuffix'
import { RepositoryService } from '../../services/RepositoryService'
import { In, Repository } from 'typeorm'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { FieldOfStudy, FieldOfStudyInput } from '../../entities/FieldOfStudy'
import { Campus, CampusInput } from '../../entities/Campus'
import { User } from '../../entities/User'

@ResolverService(School)
export class SchoolResolver implements AfterDBInit {
	private schoolRepo!: Repository<School>
	private campusRepo!: Repository<Campus>
	private fieldOfStudyRepo!: Repository<FieldOfStudy>
	constructor(private repoService: RepositoryService) {
	}

	$afterDBInit() {
		this.schoolRepo = this.repoService.getRepository(School)
		this.fieldOfStudyRepo = this.repoService.getRepository(FieldOfStudy)
		this.campusRepo = this.repoService.getRepository(Campus)
	}

	@Query(returns => [School])
	async schools(): Promise<School[]> {
		return this.schoolRepo.find()
	}

	@Query(type => School)
	school(@Arg('schoolId') schoolId: string) {
		const school = this.schoolRepo.findOne(schoolId)
		if (school === undefined) {
			throw new EntityNotFoundError(School, schoolId)
		}

		return school
	}

	@Mutation(type => School)
	async createSchool(
		@Arg('name') name: string,
		@Arg('campuses', type => [CampusInput]) campuses: CampusInput[],
		@Arg('fieldsOfStudies', type => [FieldOfStudyInput]) fieldsOfStudies: FieldOfStudyInput[],
		@Arg('emailSuffixes', type => [EmailSuffixInput]) emailSuffixes: EmailSuffixInput[]) {
		try {
			const school = new School()
			school.name = name
			school.emailSuffixes = emailSuffixes.map(suffix => inputMapper(suffix, EmailSuffix))
			school.campuses = campuses.map(campus => inputMapper(campus, Campus))

			school.fieldsOfStudies = fieldsOfStudies.map(({ school, ...fieldOfStudy }) => inputMapper(fieldOfStudy, FieldOfStudy))
			return this.schoolRepo.save(school)
		} catch (e) {
			console.log(e)
		}
	}

	@Mutation(type => School)
	async updateSchool(
		@Arg('id') id: string,
		@Arg('name') name: string,
		@Arg('campuses', type => [CampusInput]) campuses: CampusInput[],
		@Arg('fieldsOfStudies', type => [FieldOfStudyInput]) fieldsOfStudies: FieldOfStudyInput[],
		@Arg('emailSuffixes', type => [EmailSuffixInput]) emailSuffixes: EmailSuffixInput[]) {
		let school = await this.schoolRepo.findOne(id)
		if (school === undefined) throw new EntityNotFoundError(School, id)
		school.name = name
		school.emailSuffixes = emailSuffixes.map(suffix => inputMapper(suffix, EmailSuffix))
		school.campuses = await Promise.all(campuses.map(async campus => {
			const oldCampus = await this.campusRepo.findOne(campus.id)
			if (oldCampus !== undefined) {
				const students = await oldCampus.students
				const appointments = await oldCampus.appointments
				if (campus.deletedAt != null && students.length === 0 && appointments.length === 0) {
					await this.campusRepo.delete(oldCampus)
					return null
				}
			}
			return inputMapper(campus, Campus)
		})).then()
		school.fieldsOfStudies = await Promise.all(fieldsOfStudies.map(async fieldOfStudy => {
			const oldStudy = await this.fieldOfStudyRepo.findOne(fieldOfStudy.id)
			if (oldStudy !== undefined) {
				const students = await oldStudy.students
				if (fieldOfStudy.deletedAt != null && await students.length === 0) {
					await this.fieldOfStudyRepo.delete(oldStudy)
					return null
				}
				return inputMapper(fieldOfStudy, FieldOfStudy)
			}
		})).then()
		return this.schoolRepo.save(school)
	}

	@Mutation(type => School)
	async deactivateSchool(@Arg('id') id: string) {
		const school = await this.schoolRepo.findOne(id)
		if (school === undefined) {
			throw new EntityNotFoundError(School, id)
		}
		school.emailSuffixes = []
		school.deletedAt = new Date()
		return this.schoolRepo.save(school)
	}

	@Mutation(type => School)
	async activateSchool(@Arg('id') id: string) {
		const school = await this.schoolRepo.findOne(id)
		if (school === undefined) {
			throw new EntityNotFoundError(School, id)
		}
		school.deletedAt = undefined
		return this.schoolRepo.save(school)
	}

	@Subscription(type => [School], { topics: 'SCHOOLS', name: 'schools' })
	async subscribeSchools(): Promise<School[]> {
		return this.schoolRepo.find()
	}

	@Authorized()
	@FieldResolver()
	async emailSuffixes(@Root() school: School) {
		return school.emailSuffixes
	}

	@Authorized()
	@FieldResolver(type => [User])
	async students(@Root() school: School) {
		return school.students
	}
}
