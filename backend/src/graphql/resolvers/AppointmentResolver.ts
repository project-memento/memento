import { ResolverService } from '@tsed/graphql'
import { Arg, Authorized, Mutation, Query } from 'type-graphql'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { In, Repository } from 'typeorm'
import { User } from '../../entities/User'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { Appointment } from '../../entities/Appointment'
import { AssistanceOffer } from '../../entities/AssistanceOffer'
import { Campus } from '../../entities/Campus'

@ResolverService(Appointment)
export class AppointmentResolver implements AfterDBInit {
	userRepo!: Repository<User>
	appointmentRepo!: Repository<Appointment>
	assistanceOfferRepo!: Repository<AssistanceOffer>
	campusRepo!: Repository<Campus>

	$afterDBInit() {
		this.userRepo = this.repoService.getRepository(User)
		this.appointmentRepo = this.repoService.getRepository(Appointment)
		this.assistanceOfferRepo = this.repoService.getRepository(AssistanceOffer)
		this.campusRepo = this.repoService.getRepository(Campus)
	}

	constructor(private repoService: RepositoryService) {
	}

	@Query(type => Appointment)
	appointment(@Arg('appointmentId') appointmentId: string): Promise<Appointment|undefined> {
		const appointment = this.appointmentRepo.findOne(appointmentId)
		if (appointment === undefined) {
			throw new EntityNotFoundError(Appointment, appointmentId)
		}

		return appointment
	}

	@Query(type => [Appointment])
	async appointments(
		@Arg('userIds', type => [String], { nullable: true }) userIds: string[],
		@Arg('date', { nullable: true }) date: Date
	): Promise<Appointment[]> {
		let conditions: any = {}
		if (userIds !== undefined && userIds.length > 0) conditions['user'] = { id: In(userIds) }
		if (date !== undefined) conditions['date'] = { date: date }
		return this.appointmentRepo.find(conditions)
	}

	@Authorized()
	@Mutation(type => Appointment)
	async createAppointment(
		@Arg('date') date: Date,
		@Arg('userIds', type => [String]) userIds: string[],
		@Arg('assistanceOfferId') assistanceOfferId: string,
		@Arg('locationId') locationId: string
	): Promise<Appointment> {
		const appointment = new Appointment(
			date,
			await this.userRepo.find({ id: In(userIds) }),
			await this.campusRepo.findOne(locationId),
			await this.assistanceOfferRepo.findOne(assistanceOfferId)
		)
		return this.appointmentRepo.save(appointment)
	}
}
