import { ResolverService } from '@tsed/graphql'
import { Query, Mutation, Arg, Ctx } from 'type-graphql'
import { inputMapper } from '../../helpers/InputMapper'
import { Email, EmailInput } from '../../entities/Email'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { EmailSuffix } from '../../entities/EmailSuffix'
import { Repository } from 'typeorm'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { School } from '../../entities/School'
import { Forbidden } from 'ts-httpexceptions'

@ResolverService(Email)
export class EmailResolver implements AfterDBInit {
	emailRepo!: Repository<Email>
	emailSuffixRepo!: Repository<EmailSuffix>
	schoolRepo!: Repository<School>

	constructor(private repoService: RepositoryService) {
	}

	$afterDBInit(): void | Promise<any> {
		this.emailRepo = this.repoService.getRepository(Email)
		this.emailSuffixRepo = this.repoService.getRepository(EmailSuffix)
		this.schoolRepo = this.repoService.getRepository(School)
	}

	@Query(type => Email)
	async email(@Arg('emailId') emailId: string, @Ctx() ctx: any) {
		const email = await this.emailRepo.findOne(emailId)
		if (email === undefined) {
			throw new EntityNotFoundError(Email, emailId)
		}

		if (email.user.id !== ctx.user.id) {
			throw new Forbidden('You are not authorized to read this resource')
		}

		return email
	}

	@Mutation(type => Email)
	async createEmail(@Arg('email') emailInput: EmailInput) {
		return this.emailRepo.save(inputMapper(emailInput, Email))
	}

	@Mutation(type => EmailSuffix)
	async createEmailSuffix(@Arg('suffix') emailSuffix: string, @Arg('schoolId') schoolId: string) {
		const suffix = new EmailSuffix(emailSuffix, await this.schoolRepo.findOne(schoolId))
		return this.emailSuffixRepo.save(suffix)
	}

	@Query(type => EmailSuffix, { nullable: true })
	async emailSuffix(@Arg('suffix') suffix: string) {
		let suff = await this.emailSuffixRepo.findOne(suffix)
		return suff
	}
}
