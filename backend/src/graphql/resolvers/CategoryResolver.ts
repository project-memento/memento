import { ResolverService } from '@tsed/graphql'
import { Query, Mutation, Arg } from 'type-graphql'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { Category } from '../../entities/Category'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { IsNull, Not, Repository } from 'typeorm'

@ResolverService(Category)
export class CategoryResolver implements AfterDBInit {
	private categoryRepo!: Repository<Category>

	$afterDBInit(): void | Promise<any> {
		this.categoryRepo = this.repoService.getRepository(Category)
	}

	constructor(private repoService: RepositoryService) {

	}

	@Query(type => Category)
	category(@Arg('categoryId') categoryId: string) {
		const category = this.categoryRepo.findOne(categoryId)
		if (category === undefined) {
			throw new EntityNotFoundError(Category, categoryId)
		}

		return category
	}

	@Query(type => [Category])
	async categories(@Arg('includeDeactivated') includeDeactivated: boolean) {
		if (includeDeactivated) {
			return this.categoryRepo.find()
		} else {
			return this.categoryRepo.manager
				.createQueryBuilder(Category, 'category')
				.where('category.deletedAt is null')
				.getMany()
		}
	}

	@Mutation(type => Category)
	async createCategory(@Arg('name') name: string) {
		return this.categoryRepo.save(new Category(name))
	}

	@Mutation(type => Category)
	async updateCategory(@Arg('id') id: string, @Arg('name') name: string) {
		const category = await this.categoryRepo.findOne(id)
		if (category === undefined) throw new EntityNotFoundError(Category, id)
		category.name = name
		return this.categoryRepo.save(category)
	}

	@Mutation(type => Category)
	async restoreCategory(@Arg('id') id: string) {
		const category = await this.categoryRepo.findOne(id)
		if (category === undefined) throw new EntityNotFoundError(Category, id)
		category.deletedAt = undefined
		await this.categoryRepo.manager.createQueryBuilder()
			.update(Category)
			.set({ deletedAt: undefined })
			.where('id = :id', { id: category.id })
			.execute()
		return category
	}

	@Mutation(type => Category)
	async deleteCategory(@Arg('id') id: string) {
		const category = await this.categoryRepo.findOne(id)
		if (category === undefined) throw new EntityNotFoundError(Category, id)
		if ((await category.posts).length > 0) {
			category.deletedAt = new Date()
			return this.categoryRepo.save(category)
		}
		this.categoryRepo.delete(category)
		return { id: '' } // Since we return two different kinds we just return empty object
	}
}
