import { ResolverService } from '@tsed/graphql'
import { Arg, Authorized, Mutation, Query, Ctx, FieldResolver, Root } from 'type-graphql'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { In, Repository } from 'typeorm'
import { User } from '../../entities/User'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { AssistanceOffer } from '../../entities/AssistanceOffer'
import { Post } from '../../entities/Post'
import { NotFound } from 'ts-httpexceptions'
import { UserService } from '../../services/UserService'
import { __await } from 'tslib'
import { Conversation } from '../../entities/Conversation'
import { ConversationParticipant } from '../../entities/ConversationParticipant'

@ResolverService(AssistanceOffer)
export class AssistanceOfferResolver implements AfterDBInit {
	private conversationParticipantRepo!: Repository<ConversationParticipant>
	userRepo!: Repository<User>
	assistanceOfferRepo!: Repository<AssistanceOffer>
	postRepo!: Repository<Post>

	$afterDBInit() {
		this.conversationParticipantRepo = this.repoService.getRepository(ConversationParticipant)
		this.userRepo = this.repoService.getRepository(User)
		this.assistanceOfferRepo = this.repoService.getRepository(AssistanceOffer)
		this.postRepo = this.repoService.getRepository(Post)
	}

	constructor(private repoService: RepositoryService, private userService: UserService) {
	}

	@Query(type => AssistanceOffer)
	assistanceOffer(@Arg('assistanceOfferId') assistanceOfferId: string): Promise<AssistanceOffer|undefined> {
		const assistanceOffer = this.assistanceOfferRepo.findOne(assistanceOfferId)
		if (assistanceOffer === undefined) {
			throw new EntityNotFoundError(AssistanceOffer, assistanceOfferId)
		}

		return assistanceOffer
	}

	@Query(type => [AssistanceOffer])
	async assistanceOffers(@Arg('userIds', type => [String]) userIds: string[]): Promise<AssistanceOffer[]> {
		let conditions: any = {}
		if (userIds !== undefined && userIds.length > 0) conditions['helper'] = { id: In(userIds) }
		return this.assistanceOfferRepo.find(conditions)
	}

	@Authorized()
	@Mutation(type => AssistanceOffer)
	async createAssistanceOffer(
		@Arg('helperId') helperId: string,
		@Arg('postId') postId: string,
		@Arg('description') description: string
	): Promise<AssistanceOffer> {
		const assistanceOffer = new AssistanceOffer(
			await this.userRepo.findOne({ id: helperId }),
			await this.postRepo.findOne({ id: postId }),
			description,
			false
		)
		return this.assistanceOfferRepo.save(assistanceOffer)
	}

	@Authorized()
	@Mutation(type => String)
	async acceptAssistanceOffer(@Arg('id') id: string, @Ctx() ctx: any) {
		const assistanceOffer = await this.assistanceOfferRepo.findOne(id)
		if (assistanceOffer === undefined) {
			return new NotFound(`Could not find assistance offer with id: ${id}`)
		}
		assistanceOffer.accepted = true
		const userId = ctx.user.id
		const conversationRepo = this.repoService.getRepository(Conversation)

		const participantIds = [ assistanceOffer.helper.id, userId ].sort()

		const conversation = await this.conversationParticipantRepo
			.find({
				where: {
					user: { id: userId }
				},
				relations: [
					'conversation'
				]
			})
			.then(async list => {
				const conversations = await Promise.all(list.map(p => p.conversation))
				return conversations.find(conversation =>
					conversation.participants.some(p => p.user.id === assistanceOffer.helper.id))
			})
			.then(conversation => {
				// @ts-ignore
				return conversation || conversationRepo.save(new Conversation(participantIds.map(id => ({ id }))))
			})
		await this.assistanceOfferRepo.save(assistanceOffer)

		// @ts-ignore
		return conversation.id
	}

	@Authorized()
	@Mutation(type => AssistanceOffer)
	async deleteAssistanceOffer(@Arg('id') id: string) {
		const assistanceOffer = await this.assistanceOfferRepo.findOne(id)
		if (assistanceOffer === undefined) {
			return new NotFound(`Could not find assistance offer with id: ${id}`)
		}
		return this.assistanceOfferRepo.delete(assistanceOffer)
	}

	@FieldResolver()
	async helper(@Root() offer: AssistanceOffer) {
		if (!offer.helper) {
			offer = await this.assistanceOfferRepo.preload(offer) as AssistanceOffer
		}
		return offer.helper
	}
}
