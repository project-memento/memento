import { ResolverService } from '@tsed/graphql'
import { Query, Mutation, Arg } from 'type-graphql'
import { Tag, TagInput } from '../../entities/Tag'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { TagService } from '../../services/TagService'
import { inputMapper } from '../../helpers/InputMapper'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { RepositoryService } from '../../services/RepositoryService'
import { Repository } from 'typeorm'

@ResolverService(Tag)
export class TagResolver implements AfterDBInit {
	private tagRepo!: Repository<Tag>

	constructor(private tagService: TagService, private repoService: RepositoryService) {

	}

	$afterDBInit() {
		this.tagRepo = this.repoService.getRepository(Tag)
	}

	@Query(type => [Tag])
	async tags(@Arg('includeDeactivated') includeDeactivated: boolean) {
		if (includeDeactivated) {
			return this.tagRepo.find()
		} else {
			return this.tagRepo.manager
				.createQueryBuilder(Tag, 'tag')
				.where('tag.deletedAt is null')
				.getMany()
		}
	}

	@Query(type => Tag)
	tag(@Arg('tagId') tagId: string) {
		const tag = this.tagService.find(tagId)
		if (tag === undefined) {
			throw new EntityNotFoundError(Tag, tagId)
		}

		return tag
	}

	@Mutation(type => Tag)
	async createTag(@Arg('tag') tag: string) {
		return this.tagService.create(new Tag(tag))
	}

	@Mutation(type => Tag)
	async updateTag(@Arg('id') id: string, @Arg('tag') tag: string) {
		const oldTag = await this.tagRepo.findOne(id)
		if (oldTag === undefined) throw new EntityNotFoundError(Tag, id)
		oldTag.tag = tag
		return this.tagRepo.save(oldTag)
	}

	@Mutation(type => Tag)
	async restoreTag(@Arg('id') id: string) {
		const tag = await this.tagRepo.findOne(id)
		if (tag === undefined) throw new EntityNotFoundError(Tag, id)
		tag.deletedAt = undefined
		await this.tagRepo.manager.createQueryBuilder()
			.update(Tag)
			.set({ deletedAt: undefined })
			.where('id = :id', { id: tag.id })
			.execute()
		return tag
	}

	@Mutation(type => Tag)
	async deleteTag(@Arg('id') id: string) {
		const tag = await this.tagRepo.findOne(id)
		if (tag === undefined) throw new EntityNotFoundError(Tag, id)
		if ((await tag.posts).length > 0) {
			tag.deletedAt = new Date()
			return this.tagRepo.save(tag)
		}
		this.tagRepo.delete(tag)
		return { id: '' } // Since we return two different kinds we just return empty object
	}
}
