import { Subscription, Root, Mutation, Args, Arg, PubSub, PubSubEngine, Resolver } from 'type-graphql'
import { TypeORMService } from '../../extensions/typeorm'
import { Connection } from 'typeorm'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import EntityFixtures from '../../fixtures/EntityFixtures'
import { RepositoryService } from '../../services/RepositoryService'
import { registerResolverService } from '@tsed/graphql/lib/registries/ResolverServiceRegistry'
import { School } from '../../entities/School'

function TestResolverService(...args: any[]): ClassDecorator {
	return <TFunction extends Function>(target: TFunction): void => {
		if (process.env.NODE_ENV === 'test') {
			// @ts-ignore
			Resolver.apply(this, args)(target)
			registerResolverService(target)
		}
	}
}

@TestResolverService(String)
export class TestResolver implements AfterDBInit {
	private connection!: Connection
	private entityFixtures!: EntityFixtures

	constructor(private typeORMService: TypeORMService, private repoService: RepositoryService) {}

	$afterDBInit(): void | Promise<any> {
		this.connection = this.typeORMService.get() as Connection
		this.entityFixtures = new EntityFixtures(this.repoService)
	}

	@Mutation(type => Boolean)
	async cleanDB(): Promise<boolean> {
		await this.connection.synchronize(true)
		return true
	}

	@Mutation(type => String)
	async fixtures(@Arg('fixtures') fixtures: string): Promise<string> {
		const deserialized = await this.entityFixtures.deserialize(JSON.parse(fixtures))
		const saved = await this.entityFixtures.save(deserialized)
		const reSerialized = this.entityFixtures.serialize(saved)
		return JSON.stringify(reSerialized)
	}
}
