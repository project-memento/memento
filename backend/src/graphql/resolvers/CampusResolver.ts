import { ResolverService } from '@tsed/graphql'
import { Query, Mutation, Arg, Authorized, FieldResolver, Root } from 'type-graphql'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { Campus } from '../../entities/Campus'
import { RepositoryService } from '../../services/RepositoryService'
import { AfterDBInit } from '../../extensions/typeorm/interfaces/AfterDBInit'
import { Repository } from 'typeorm'
import { School } from '../../entities/School'
import { User } from '../../entities/User'

@ResolverService(Campus)
export class CampusResolver implements AfterDBInit {
	private campusRepo!: Repository<Campus>
	private schoolRepo!: Repository<School>
	constructor(private repoService: RepositoryService) {

	}

	$afterDBInit() {
		this.campusRepo = this.repoService.getRepository(Campus)
		this.schoolRepo = this.repoService.getRepository(School)
	}

	@Query(type => Campus)
	async campus(@Arg('campusId') campusId: string) {
		const campus = await this.campusRepo.findOne(campusId)
		if (campus === undefined) {
			throw new EntityNotFoundError(Campus, campusId)
		}

		return campus
	}

	@Query(type => [Campus])
	campuses(@Arg('schoolId') schoolId: string) {
		return this.campusRepo.find({ school: { id: schoolId } })
	}

	@Mutation(type => Campus)
	async createCampus(@Arg('name') name: string, @Arg('address') address: string, @Arg('schoolId') schoolId: string) {
		const school = await this.schoolRepo.findOne(schoolId)
		if (school === undefined) throw new EntityNotFoundError(School, schoolId)
		return this.campusRepo.save(new Campus(name, address, school))
	}

	@Mutation(type => Campus)
	async deleteCampus(@Arg('id') id: string) {
		const campus = await this.campusRepo.findOne(id)
		if (campus === undefined) throw new EntityNotFoundError(Campus, id)
		return this.campusRepo.remove(campus)
	}

	@Authorized()
	@FieldResolver(type => [User])
	async students(@Root() campus: Campus) {
		return campus.students
	}
}
