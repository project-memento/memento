import PaginatedResponse from './PaginatedResponse'
import { Message } from '../../entities/Message'

export const PaginatedMessageResponse = PaginatedResponse(Message)
export type PaginatedMessageResponse = InstanceType<typeof PaginatedMessageResponse>
