import { ClassType, Field, Int, ObjectType } from 'type-graphql'

export default function PaginatedResponse<T>(itemClass: ClassType<T>) {
	@ObjectType(`Paginated${itemClass.name}Response`)
	class PaginatedResponseClass {
		@Field(type => [itemClass])
		items!: T[]

		@Field(type => Int)
		total!: number

		@Field(type => Boolean)
		hasMore!: boolean
	}
	return PaginatedResponseClass
}
