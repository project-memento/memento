export type Lazy<T extends any> = Promise<T> | T
