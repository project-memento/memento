export type Constructable<T> = {
	new (): T
}

let i = 0
export const inputMapper = <T, V extends Partial<T>> (input : V, Cls: Constructable<T>) => {
	const instance: T = new Cls()
	Object.entries(input)
		.forEach(([key, value]) => {
			// @ts-ignore
			instance[key] = value
		})
	return instance
}
