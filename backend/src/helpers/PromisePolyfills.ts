export function any(promises: Promise<any>[]) : Promise<any> {
	let count = 0
	// eslint-disable-next-line promise/param-names
	return new Promise<any>((resolve, _reject) => {
		const reject = () => {
			if (++count === promises.length) {
				// eslint-disable-next-line prefer-promise-reject-errors
				_reject()
			}
		}

		promises.forEach(promise => {
			promise.then(resolve, reject)
		})
	})
}
