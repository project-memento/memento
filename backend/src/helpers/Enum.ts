import { ColumnMetadataArgs } from 'typeorm/metadata-args/ColumnMetadataArgs'
import { ColumnOptions, getMetadataArgsStorage } from 'typeorm'

export function Enum(enumType: any[] | Object, options: ColumnOptions = {}) {
	return function (object: Object, propertyName: string) {
		options.type = 'enum'
		if (process.env.NODE_ENV === 'test') {
			options.type = 'varchar'
			options.transformer = {
				// @ts-ignore
				to: value => enumType[value],
				// @ts-ignore
				from: value => enumType[value]
			}
		} else {
			options.enum = enumType
		}

		// register a regular column
		getMetadataArgsStorage().columns.push({
			target: object.constructor,
			propertyName: propertyName,
			mode: 'regular',
			options: options
		} as ColumnMetadataArgs)
	}
}
