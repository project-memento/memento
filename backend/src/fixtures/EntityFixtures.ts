import 'reflect-metadata'
import 'object.fromentries/auto'
import * as Faker from 'faker'
import { RepositoryService } from '../services/RepositoryService'
import { School } from '../entities/School'
import { EmailSuffix } from '../entities/EmailSuffix'
import { Constructable, inputMapper } from '../helpers/InputMapper'
import { Campus } from '../entities/Campus'
import { Tag } from '../entities/Tag'
import { FieldOfStudy } from '../entities/FieldOfStudy'
import { User } from '../entities/User'
import { UserProfile } from '../entities/UserProfile'
import { StudentYear } from '../entities/StudentYear'
import { Email } from '../entities/Email'
import { Lazy } from 'src/helpers/Lazy'
import { Appointment } from '../entities/Appointment'
import { AssistanceOffer } from '../entities/AssistanceOffer'
import { Post } from '../entities/Post'
import { Category } from '../entities/Category'
import { Role } from '../entities/Role'

type LazyProp<W> = W extends Lazy<infer A>
	? A extends (infer T)[]
		? Lazy<T>[] | Lazy<T[]> | (() => Lazy<T>[] | Lazy<T[]>)
		: Lazy<A> | (() => Lazy<A>)
	: never

type LazyProps<T> = {
	[K in keyof T]: LazyProp<T[K]>
}

async function mergeOptions<T, K = 'id'>(defaults: LazyProps<Pick<T, Exclude<keyof T, K>>>, input: Partial<T>, cls: Constructable<T>): Promise<T> {
	const options = { ...defaults, ...input }

	const parsedOptions = await Promise.all(Object.entries(options)
		.map(async ([key, value]) => {
			if (typeof value === 'function') {
				value = value()
			}
			if (Array.isArray(value)) {
				value = Promise.all(value)
			}
			value = await Promise.resolve(value)
			return [key, value]
		})
	) as [string, any][]

	return inputMapper(
		Object.fromEntries(
			parsedOptions
				.filter(([_, value]) => {
					switch (typeof value) {
					case 'undefined':
						return false
					case 'object':
						if (Array.isArray(value)) {
							return value.length > 0
						}
						return true
					default:
						return true
					}
				})
		) as T,
		cls
	)
}

function Save(cls: any) {
	return (target: EntityFixtures, propertyKey: string, descriptor: TypedPropertyDescriptor<(options: any) => any>) => {
		if (!target.hasOwnProperty('mapping')) {
			// @ts-ignore
			target.mapping = {}
		}
		// @ts-ignore
		target.mapping[propertyKey] = cls
		return {
			value: async function (this: any, ...args: any[]) {
				const shouldSave = this._save
				if (shouldSave) {
					this._save = false
				}
				// @ts-ignore
				const value = await descriptor.value.call(this, ...args)
				if (typeof this.repoService === 'undefined' || this.repoService === null) {
					return this.serialize(value)
				}
				if (shouldSave) {
					this._save = true
					const repo = this.repoService.getRepository(value.constructor)
					try {
						return repo.save(value)
					} catch (e) {
						console.log(e)
					}
				}
				return value
			} as any,
			writable: true,
			enumerable: false,
			configurable: true
		}
	}
}

function getRandomInt(max: number) {
	return Math.floor(Math.random() * Math.floor(max))
}

class EntityFixtures {
	private _save = true
	private mapping!: any

	constructor(private repoService: RepositoryService | null = null) {
	}

	serialize(input: any): object {
		return Object.fromEntries([
			['__cls', input.constructor.name],
			...Object.entries(Object.getOwnPropertyDescriptors(input))
				.map(([key, descriptor]) => [key, descriptor.value]) as [string, any]
		])
	}

	deserialize(input: any): any {
		if (Array.isArray(input)) {
			return input.map(value => this.deserialize.bind(this)(value))
		}
		if (input !== null && typeof input === 'object' && input.hasOwnProperty('__cls')) {
			if (this.mapping.hasOwnProperty(input.__cls)) {
				const { __cls, ...sanitized } = input
				return inputMapper(
					Object.fromEntries(
						Object.entries(sanitized)
							.map(([key, value]: [string, any]) => [key, this.deserialize.bind(this)(value)])
					),
					this.mapping[__cls]
				)
			}
		}
		return input
	}

	async save<T>(input: T): Promise<T> {
		if (typeof this.repoService === 'undefined' || this.repoService === null) {
			throw new Error('No repoService found')
		}
		return this.repoService.getRepository(input.constructor).save(input)
	}

	@Save(EmailSuffix)
	async EmailSuffix(options: Partial<EmailSuffix> = {}): Promise<EmailSuffix> {
		return mergeOptions(
			{
				suffix: Faker.internet.domainName(),
				school: () => this.School({ emailSuffixes: [] })
			},
			options,
			EmailSuffix
		)
	}

	@Save(Campus)
	async Campus(options: Partial<Campus> = {}): Promise<Campus> {
		return mergeOptions(
			{
				name: 'Fjerdingen',
				address: 'Christan Krogsgate 32',
				school: () => this.School({ campuses: [] }),
				students: [],
				appointments: [],
				deletedAt: undefined
			},
			options,
			Campus
		)
	}

	@Save(FieldOfStudy)
	async FieldOfStudy(options: Partial<FieldOfStudy> = {}): Promise<FieldOfStudy> {
		return mergeOptions(
			{
				name: 'Programming',
				students: [],
				school: () => this.School({ fieldsOfStudies: [] }),
				deletedAt: undefined
			},
			options,
			FieldOfStudy
		)
	}

	@Save(School)
	async School(options: Partial<School> = {}): Promise<School> {
		return mergeOptions(
			{
				name: 'Westerdals',
				emailSuffixes: () => [
					this.EmailSuffix({ suffix: 'student.kristiania.no', school: undefined }),
					this.EmailSuffix({ suffix: 'student.westerdals.no', school: undefined })
				],
				campuses: () => [
					this.Campus({ school: undefined })
				],
				fieldsOfStudies: () => [
					this.FieldOfStudy({ name: 'Programming', school: undefined }),
					this.FieldOfStudy({ name: 'Mobile development', school: undefined }),
				],
				students: [],
				deletedAt: undefined
			},
			options,
			School
		)
	}

	@Save(Tag)
	async Tag(options: Partial<Tag> = {}): Promise<Tag> {
		return mergeOptions(
			{
				tag: Faker.hacker.noun(),
				users: [],
				posts: []
			},
			options,
			Tag
		)
	}

	@Save(UserProfile)
	async UserProfile(options: Partial<UserProfile> = {}): Promise<UserProfile> {
		return mergeOptions(
			{
				firstname: Faker.name.firstName(),
				lastname: Faker.name.lastName(),
				imageLink: Faker.image.imageUrl(),
				// @ts-ignore
				currentYear: StudentYear[StudentYear[getRandomInt(4)]],
				user: () => this.User({ profile: undefined })
			},
			options,
			UserProfile
		)
	}

	@Save(Email)
	async Email(options: Partial<Email> = {}): Promise<Email> {
		return mergeOptions(
			{
				email: Faker.internet.email(),
				isPrimary: true,
				isVerified: false,
				user: () => this.User({ emails: [] }),
			},
			options,
			Email
		)
	}

	@Save(User)
	async User(options: Partial<User> = {}): Promise<User> {
		const school : School = await Promise.resolve(options.school instanceof School
			? options.school
			: this.School())
		delete options.school
		delete options.fieldOfStudy
		delete options.campuses

		return mergeOptions(
			{
				password: 'Password123',
				profile: () => this.UserProfile({ user: undefined }),
				emails: () => [
					this.Email({ user: undefined })
				],
				skills: () => [
					this.Tag()
				],
				// @ts-ignore
				fieldOfStudy: () => school.fieldsOfStudies[0],
				school: () => school,
				posts: [],
				campuses: () => school.campuses,
				conversations: [],
				appointments: [],
				role: Role.USER
			},
			options,
			User
		)
	}

	@Save(Appointment)
	async Appointment(options: Partial<Appointment> = {}): Promise<Appointment> {
		return mergeOptions(
			{
				users: () => [
					this.User(),
					this.User()
				],
				date: new Date(),
				assistanceOffer: () => this.AssistanceOffer({ appointment: undefined }),
				location: () => this.Campus()
			},
			options,
			Appointment
		)
	}

	@Save(AssistanceOffer)
	async AssistanceOffer(options: Partial<AssistanceOffer> = {}): Promise<AssistanceOffer> {
		return mergeOptions(
			{
				post: () => this.Post({ assistanceOffers: [] }),
				description: Faker.lorem.sentence(),
				accepted: false,
				helper: () => this.User({ emails: [] }),
				appointment: () => this.Appointment({ assistanceOffer: undefined })
			},
			options,
			AssistanceOffer
		)
	}

	@Save(Category)
	async Category(options: Partial<Category> = {}): Promise<Category> {
		return mergeOptions(
			{
				name: () => Faker.lorem.word(),
				posts: [],
				deletedAt: undefined
			},
			options,
			Category
		)
	}

	@Save(Post)
	async Post(options: Partial<Post> = {}): Promise<Post> {
		const user = await Promise.resolve(options.user instanceof User ? options.user : this.User())
		const category = await Promise.resolve(options.category instanceof Category ? options.category : this.Category({ posts: [] }))
		const tags = await Promise.resolve(options.tags instanceof Array ? options.tags : [this.Tag()])
		const campus = user.campuses[0]
		delete options.user
		delete options.category
		delete options.tags
		return mergeOptions(
			{
				title: Faker.lorem.words(3),
				content: Faker.lorem.sentence(),
				category: () => category,
				deadline: Faker.date.future(0.1),
				user: () => user,
				location: () => campus,
				tags: () => tags,
				imageLink: Faker.internet.domainName(),
				assistanceOffers: []
			},
			options,
			Post
		)
	}
}

export default EntityFixtures
