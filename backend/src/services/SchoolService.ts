import { OnInit, Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { School } from '../entities/School'
import { TypeORMService } from '../extensions/typeorm'
import { AfterDBInit } from '../extensions/typeorm/interfaces/AfterDBInit'

@Service()
export class SchoolService implements AfterDBInit {
	private connection!: Connection
	private repository!: Repository<School>

	constructor(private typeORMService: TypeORMService) {}

	$afterDBInit() {
		this.connection = this.typeORMService.get() as Connection
		this.repository = this.connection.manager.getRepository(School)
	}

	async find(id: string): Promise<School|undefined> {
		return this.repository.findOne(id)
	}

	async create(school: School): Promise<School> {
		return this.repository.save(school)
	}
}
