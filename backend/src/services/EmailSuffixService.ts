import { OnInit, Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { EmailSuffix } from '../entities/EmailSuffix'
import { TypeORMService } from '../extensions/typeorm'
import { AfterDBInit } from '../extensions/typeorm/interfaces/AfterDBInit'

@Service()
export class EmailSuffixService implements AfterDBInit {
	private connection!: Connection
	private repository!: Repository<EmailSuffix>

	constructor(private typeORMService: TypeORMService) {}

	$afterDBInit() {
		this.connection = this.typeORMService.get() as Connection
		this.repository = this.connection.manager.getRepository(EmailSuffix)
	}

	async find(id: string): Promise<EmailSuffix|undefined> {
		return this.repository.findOne(id)
	}

	async exists(id: string): Promise<EmailSuffix> {
		return this.repository.findOneOrFail(id)
	}

	async create(emailSuffix: EmailSuffix): Promise<EmailSuffix> {
		return this.repository.save(emailSuffix)
	}
}
