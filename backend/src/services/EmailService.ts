import { Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { Email } from '../entities/Email'
import { AfterDBInit } from '../extensions/typeorm/interfaces/AfterDBInit'
import { TypeORMService } from '../extensions/typeorm'

@Service()
export class EmailService implements AfterDBInit {
	private connection!: Connection
	private repository!: Repository<Email>

	constructor(private typeORMService: TypeORMService) {
	}

	async $afterDBInit() {
		this.connection = await this.typeORMService.get() as Connection
		this.repository = await this.connection.manager.getRepository(Email)
	}

	async find(id: string): Promise<Email|undefined> {
		return this.repository.findOne(id)
	}

	async exists(id: string): Promise<Email> {
		return this.repository.findOneOrFail(id)
	}

	async create(email: Email): Promise<Email> {
		return this.repository.save(email)
	}
}
