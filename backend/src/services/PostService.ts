import { AfterRoutesInit, Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { Post, PostInput } from '../entities/Post'
import { TypeORMService } from '../extensions/typeorm'

@Service()
export class PostService implements AfterRoutesInit {
	private connection!: Connection
	private repository!: Repository<Post>

	constructor(private typeORMService: TypeORMService) {}

	$afterRoutesInit() {
		this.connection = this.typeORMService.get() as Connection
		this.repository = this.connection.manager.getRepository(Post)
	}

	async find(id: string): Promise<Post|undefined> {
		return this.repository.findOne(id)
	}

	async create(post: Post): Promise<Post> {
		return this.repository.save(post)
	}
}
