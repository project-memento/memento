import { OnInit, Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { Category } from '../entities/Category'
import { TypeORMService } from '../extensions/typeorm'
import { AfterDBInit } from '../extensions/typeorm/interfaces/AfterDBInit'

@Service()
export class CategoryService implements AfterDBInit {
	private connection!: Connection
	private repository!: Repository<Category>

	constructor(private typeORMService: TypeORMService) {}

	$afterDBInit() {
		this.connection = this.typeORMService.get() as Connection
		this.repository = this.connection.manager.getRepository(Category)
	}

	async find(id: string): Promise<Category|undefined> {
		return this.repository.findOne(id)
	}

	async create(category: Category): Promise<Category> {
		return this.repository.save(category)
	}
}
