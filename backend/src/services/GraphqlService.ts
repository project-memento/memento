import { AfterRoutesInit, Service } from '@tsed/common'
import { GraphQLService as graphqlService } from '@tsed/graphql'
import { ApolloServer } from 'apollo-server-express'

@Service()
export class GraphqlService implements AfterRoutesInit {
	private server!: ApolloServer
	constructor(private graphqlService: graphqlService) {}

	$afterRoutesInit() {
		this.server = this.graphqlService.get() as ApolloServer
	}
}
