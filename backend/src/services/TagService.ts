import { OnInit, Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { Tag } from '../entities/Tag'
import { TypeORMService } from '../extensions/typeorm'
import { AfterDBInit } from '../extensions/typeorm/interfaces/AfterDBInit'

@Service()
export class TagService implements AfterDBInit {
	private connection!: Connection
	private repository!: Repository<Tag>

	constructor(private typeORMService: TypeORMService) {}

	$afterDBInit() {
		this.connection = this.typeORMService.get() as Connection
		this.repository = this.connection.manager.getRepository(Tag)
	}

	async find(id: string): Promise<Tag|undefined> {
		return this.repository.findOne(id)
	}

	async create(tag: Tag): Promise<Tag> {
		return this.repository.save(tag)
	}
}
