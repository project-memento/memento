import { Service, Constant } from '@tsed/common'
import { User } from '../entities/User'
import { Repository } from 'typeorm'
import { hashSync, compareSync } from 'bcryptjs'
import { AfterDBInit } from '../extensions/typeorm/interfaces/AfterDBInit'
import { sign } from 'jsonwebtoken'
import { RepositoryService } from './RepositoryService'
import { Email } from '../entities/Email'
import { NotFound } from 'ts-httpexceptions'
import { Conversation } from '../entities/Conversation'

@Service()
export class UserService implements AfterDBInit {
	private repository!: Repository<User>
	private conversationRepo!: Repository<Conversation>

	@Constant('jwt.secretOrKey')
	private jwtSecret!: string

	constructor(private repoService: RepositoryService) {}

	async $afterDBInit() {
		this.repository = await this.repoService.getRepository(User)
		this.conversationRepo = await this.repoService.getRepository(Conversation)
	}

	async find(id: any): Promise<User> {
		return this.repository.findOneOrFail(id)
	}

	async create(user: User): Promise<User> {
		user.password = hashSync(user.password, 10)
		return this.repository.save(user)
	}

	async login(email: string, password: string) {
		try {
			const { user } = await this.repoService.getRepository(Email).findOneOrFail(email, {
				relations: ['user']
			})
			if (user && compareSync(password, user.password)) {
				return sign({
				}, this.jwtSecret, {
					audience: 'connect.app',
					issuer: 'connect.app',
					subject: user.id
				})
			}
		} catch (_) {
		}
		throw new Error('Invalid user/password')
	}

	async getConversations(userId: string) {
		const ids = await this.conversationRepo
			.createQueryBuilder('conversations')
			.leftJoinAndSelect('conversations.participants', 'participants')
			.where('participants.id=:id')
			.setParameter('id', userId)
			.select('conversations.id')
			.getMany()
		
		return this.conversationRepo.findByIds(ids.map(conversation => conversation.id))
	}
}
