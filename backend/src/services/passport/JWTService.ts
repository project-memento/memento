import { BeforeRoutesInit, ExpressApplication, Inject, ServerSettingsService, Service } from '@tsed/common'
import { UserService } from '../UserService'
import * as Passport from 'passport'
import { ExtractJwt, Strategy as JwtStrategy, StrategyOptions } from 'passport-jwt'

@Service()
export class JWTService implements BeforeRoutesInit {
	constructor(
		private settings: ServerSettingsService,
		private userService: UserService,
		@Inject(ExpressApplication) private app: ExpressApplication) {
	}

	$beforeRoutesInit(): void | Promise<any> {
		const opts : StrategyOptions = {
			...this.settings.get('jwt'),
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
		}
		Passport.use(new JwtStrategy(opts, async (payload, done) => {
			try {
				const user = await this.userService.find(payload.sub)
				done(null, user)
			} catch (e) {
				done(e, null)
			}
		}))
	}
}
