import { Constant, Service } from '@tsed/di'
import { generateBlobSASQueryParameters, SharedKeyCredential } from '@azure/storage-blob'
import { ServerSettingsService } from '@tsed/common'
const spacetime = require('spacetime')

@Service()
export class AzureBlobService {
	private credential!: SharedKeyCredential
	@Constant('azureBlobStorage.containerName')
	containerName!: string

	constructor(serverSettingsService: ServerSettingsService) {
		const accessKey = serverSettingsService.get<string>('azureBlobStorage.accountKey')
		const accountName = serverSettingsService.get<string>('azureBlobStorage.accountName')
		try {
			this.credential = new SharedKeyCredential(accountName, accessKey)
		} catch (_) {}
	}

	generateSharedAccessSignature(blobName: string): string {
		if (!this.credential) {
			return ''
		}
		const expiryTime = spacetime.now().add(60, 'minutes').d

		return generateBlobSASQueryParameters(
			{
				containerName: this.containerName,
				expiryTime,
				permissions: 'r',
				blobName
			},
			this.credential
		).toString()
	}
}
