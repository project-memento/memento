import { MigrationInterface, QueryRunner } from 'typeorm'

export class addConversationAndMessageTables1559243525037 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('CREATE TABLE "message" ("id" character varying(26) NOT NULL, "message" character varying NOT NULL, "sentAt" TIMESTAMP NOT NULL DEFAULT now(), "conversationId" uuid, "authorId" uuid, CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY ("id"))')
		await queryRunner.query('CREATE TABLE "conversation" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), CONSTRAINT "PK_864528ec4274360a40f66c29845" PRIMARY KEY ("id"))')
		await queryRunner.query('CREATE TABLE "conversation_participants_user" ("conversationId" uuid NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_25e9241137cdb0f2336d267cc99" PRIMARY KEY ("conversationId", "userId"))')
		await queryRunner.query('CREATE INDEX "IDX_4928ef292e3fb48783034b82f7" ON "conversation_participants_user" ("conversationId") ')
		await queryRunner.query('CREATE INDEX "IDX_5d93fb1843f96fbdefea37dae8" ON "conversation_participants_user" ("userId") ')
		await queryRunner.query('ALTER TABLE "message" ADD CONSTRAINT "FK_7cf4a4df1f2627f72bf6231635f" FOREIGN KEY ("conversationId") REFERENCES "conversation"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "message" ADD CONSTRAINT "FK_c72d82fa0e8699a141ed6cc41b3" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "conversation_participants_user" ADD CONSTRAINT "FK_4928ef292e3fb48783034b82f7a" FOREIGN KEY ("conversationId") REFERENCES "conversation"("id") ON DELETE CASCADE ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "conversation_participants_user" ADD CONSTRAINT "FK_5d93fb1843f96fbdefea37dae86" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "conversation_participants_user" DROP CONSTRAINT "FK_5d93fb1843f96fbdefea37dae86"')
		await queryRunner.query('ALTER TABLE "conversation_participants_user" DROP CONSTRAINT "FK_4928ef292e3fb48783034b82f7a"')
		await queryRunner.query('ALTER TABLE "message" DROP CONSTRAINT "FK_c72d82fa0e8699a141ed6cc41b3"')
		await queryRunner.query('ALTER TABLE "message" DROP CONSTRAINT "FK_7cf4a4df1f2627f72bf6231635f"')
		await queryRunner.query('DROP INDEX "IDX_5d93fb1843f96fbdefea37dae8"')
		await queryRunner.query('DROP INDEX "IDX_4928ef292e3fb48783034b82f7"')
		await queryRunner.query('DROP TABLE "conversation_participants_user"')
		await queryRunner.query('DROP TABLE "conversation"')
		await queryRunner.query('DROP TABLE "message"')
	}
}
