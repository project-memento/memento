import { MigrationInterface, QueryRunner } from 'typeorm'

export class addAppointmentAndAssistanceoffer1559241664668 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('CREATE TABLE "assistance_offer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "helperId" uuid, "postId" uuid, CONSTRAINT "PK_fb763dae28c73720f29bda410da" PRIMARY KEY ("id"))')
		await queryRunner.query('CREATE TABLE "appointment" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "date" TIMESTAMP NOT NULL, "usersId" uuid, "locationId" uuid, CONSTRAINT "PK_e8be1a53027415e709ce8a2db74" PRIMARY KEY ("id"))')
		await queryRunner.query('ALTER TABLE "assistance_offer" ADD CONSTRAINT "FK_4a9b94ed0e29f06137cac0a46e0" FOREIGN KEY ("helperId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "assistance_offer" ADD CONSTRAINT "FK_1a6bee47cc12140c7fd7b2ee48d" FOREIGN KEY ("postId") REFERENCES "post"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "appointment" ADD CONSTRAINT "FK_1bfd4e75ae30ef043b420785c87" FOREIGN KEY ("usersId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "appointment" ADD CONSTRAINT "FK_70d5b11b39223725cc36e11280a" FOREIGN KEY ("locationId") REFERENCES "campus"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "appointment" DROP CONSTRAINT "FK_70d5b11b39223725cc36e11280a"')
		await queryRunner.query('ALTER TABLE "appointment" DROP CONSTRAINT "FK_1bfd4e75ae30ef043b420785c87"')
		await queryRunner.query('ALTER TABLE "assistance_offer" DROP CONSTRAINT "FK_1a6bee47cc12140c7fd7b2ee48d"')
		await queryRunner.query('ALTER TABLE "assistance_offer" DROP CONSTRAINT "FK_4a9b94ed0e29f06137cac0a46e0"')
		await queryRunner.query('DROP TABLE "appointment"')
		await queryRunner.query('DROP TABLE "assistance_offer"')
	}
}
