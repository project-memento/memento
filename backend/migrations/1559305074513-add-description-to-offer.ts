import { MigrationInterface, QueryRunner } from 'typeorm'

export class addDescriptionToOffer1559305074513 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "assistance_offer" ADD "description" character varying')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "assistance_offer" DROP COLUMN "description"')
	}
}
