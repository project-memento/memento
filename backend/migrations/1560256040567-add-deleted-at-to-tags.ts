import { MigrationInterface, QueryRunner } from 'typeorm'

export class addDeletedAtToTags1560256040567 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "tag" ADD "deletedAt" TIMESTAMP')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "tag" DROP COLUMN "deletedAt"')
	}
}
