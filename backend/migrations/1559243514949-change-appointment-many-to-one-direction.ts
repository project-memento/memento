import { MigrationInterface, QueryRunner } from 'typeorm'

export class changeAppointmentManyToOneDirection1559243514949 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "appointment" DROP CONSTRAINT "FK_1bfd4e75ae30ef043b420785c87"')
		await queryRunner.query('CREATE TABLE "appointment_users_user" ("appointmentId" uuid NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_6b74aed83e5db9e97d9434a2e57" PRIMARY KEY ("appointmentId", "userId"))')
		await queryRunner.query('CREATE INDEX "IDX_6075a79518f01eb8233144c067" ON "appointment_users_user" ("appointmentId") ')
		await queryRunner.query('CREATE INDEX "IDX_88ac9bb2d40d6cafc6fa896efb" ON "appointment_users_user" ("userId") ')
		await queryRunner.query('ALTER TABLE "appointment" DROP COLUMN "usersId"')
		await queryRunner.query('ALTER TABLE "appointment_users_user" ADD CONSTRAINT "FK_6075a79518f01eb8233144c0671" FOREIGN KEY ("appointmentId") REFERENCES "appointment"("id") ON DELETE CASCADE ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "appointment_users_user" ADD CONSTRAINT "FK_88ac9bb2d40d6cafc6fa896efb8" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "appointment_users_user" DROP CONSTRAINT "FK_88ac9bb2d40d6cafc6fa896efb8"')
		await queryRunner.query('ALTER TABLE "appointment_users_user" DROP CONSTRAINT "FK_6075a79518f01eb8233144c0671"')
		await queryRunner.query('ALTER TABLE "appointment" ADD "usersId" uuid')
		await queryRunner.query('DROP INDEX "IDX_88ac9bb2d40d6cafc6fa896efb"')
		await queryRunner.query('DROP INDEX "IDX_6075a79518f01eb8233144c067"')
		await queryRunner.query('DROP TABLE "appointment_users_user"')
		await queryRunner.query('ALTER TABLE "appointment" ADD CONSTRAINT "FK_1bfd4e75ae30ef043b420785c87" FOREIGN KEY ("usersId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
	}
}
