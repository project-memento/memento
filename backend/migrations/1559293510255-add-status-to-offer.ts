import { MigrationInterface, QueryRunner } from 'typeorm'

export class addStatusToOffer1559293510255 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "assistance_offer" ADD "accepted" boolean NOT NULL')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "assistance_offer" DROP COLUMN "accepted"')
	}
}
