import { MigrationInterface, QueryRunner } from 'typeorm'

export class addRoleToUser1559645968335 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('CREATE TYPE "user_role_enum" AS ENUM(\'0\', \'1\')')
		await queryRunner.query('ALTER TABLE "user" ADD "role" "user_role_enum" NOT NULL DEFAULT \'0\'')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "user" DROP COLUMN "role"')
		await queryRunner.query('DROP TYPE "user_role_enum"')
	}
}
