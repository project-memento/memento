import { MigrationInterface, QueryRunner } from 'typeorm'
import { User } from '../src/entities/User'
import { Email } from '../src/entities/Email'
import { Role } from '../src/entities/Role'
import { inputMapper } from '../src/helpers/InputMapper'
import { UserProfile } from '../src/entities/UserProfile'
import { hashSync } from 'bcryptjs'

export class addAdminUser1560158270936 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "user" ALTER COLUMN "role" DROP DEFAULT')

		const user = new User()
		user.password = hashSync('supersecure', 10)
		user.emails = [new Email('admin@connect.university')]
		user.role = Role.ADMIN
		user.profile = inputMapper({ firstname: 'Admin', lastname: 'Connect', imageLink: '/uploads/default.png' }, UserProfile)

		await queryRunner.manager.save(user)
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "user" ALTER COLUMN "role" SET DEFAULT \'0\'')
	}
}
