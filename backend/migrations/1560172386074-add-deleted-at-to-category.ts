import { MigrationInterface, QueryRunner } from 'typeorm'

export class addDeletedAtToCategory1560172386074 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "category" ADD "deletedAt" TIMESTAMP')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "category" DROP COLUMN "deletedAt"')
	}
}
