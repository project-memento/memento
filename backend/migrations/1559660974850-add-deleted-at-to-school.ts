import { MigrationInterface, QueryRunner } from 'typeorm'

export class addDeletedAtToSchool1559660974850 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "school" ADD "deletedAt" TIMESTAMP')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "school" DROP COLUMN "deletedAt"')
	}
}
