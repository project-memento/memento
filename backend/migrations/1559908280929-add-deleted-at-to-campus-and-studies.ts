import { MigrationInterface, QueryRunner } from 'typeorm'

export class addDeletedAtToCampusAndStudies1559908280929 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "field_of_study" ADD "deletedAt" TIMESTAMP')
		await queryRunner.query('ALTER TABLE "campus" ADD "deletedAt" TIMESTAMP')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "campus" DROP COLUMN "deletedAt"')
		await queryRunner.query('ALTER TABLE "field_of_study" DROP COLUMN "deletedAt"')
	}
}
