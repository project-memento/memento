import { MigrationInterface, QueryRunner } from 'typeorm'

export class addConversationParticipantPivotTable1560449183796 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('CREATE TABLE "conversation_participant" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "conversationId" uuid, "userId" uuid, "lastReadMessageId" character varying(26), CONSTRAINT "PK_326cbb9ee8891f8e29157bda911" PRIMARY KEY ("id"))')
		await queryRunner.query('CREATE UNIQUE INDEX "IDX_497b032725751e1aad8a85832a" ON "conversation_participant" ("conversationId", "userId") ')
		await queryRunner.query('ALTER TABLE "message" ALTER COLUMN "sentAt" DROP DEFAULT')
		await queryRunner.query('ALTER TABLE "conversation_participant" ADD CONSTRAINT "FK_b1a75fd6cdb0ab0a82c5b01c34f" FOREIGN KEY ("conversationId") REFERENCES "conversation"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "conversation_participant" ADD CONSTRAINT "FK_dd90174e375c888d7f431cf829e" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
		await queryRunner.query('ALTER TABLE "conversation_participant" ADD CONSTRAINT "FK_19b70c886c2eed6ca82a532a8f1" FOREIGN KEY ("lastReadMessageId") REFERENCES "message"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "conversation_participant" DROP CONSTRAINT "FK_19b70c886c2eed6ca82a532a8f1"')
		await queryRunner.query('ALTER TABLE "conversation_participant" DROP CONSTRAINT "FK_dd90174e375c888d7f431cf829e"')
		await queryRunner.query('ALTER TABLE "conversation_participant" DROP CONSTRAINT "FK_b1a75fd6cdb0ab0a82c5b01c34f"')
		await queryRunner.query('ALTER TABLE "message" ALTER COLUMN "sentAt" SET DEFAULT now()')
		await queryRunner.query('DROP INDEX "IDX_497b032725751e1aad8a85832a"')
		await queryRunner.query('DROP TABLE "conversation_participant"')
	}
}
