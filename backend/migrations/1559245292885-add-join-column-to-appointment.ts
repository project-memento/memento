import { MigrationInterface, QueryRunner } from 'typeorm'

export class addJoinColumnToAppointment1559245292885 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "appointment" ADD "assistanceOfferId" uuid')
		await queryRunner.query('ALTER TABLE "appointment" ADD CONSTRAINT "UQ_907208f6adb9982267492cc3946" UNIQUE ("assistanceOfferId")')
		await queryRunner.query('ALTER TABLE "appointment" ADD CONSTRAINT "FK_907208f6adb9982267492cc3946" FOREIGN KEY ("assistanceOfferId") REFERENCES "assistance_offer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "appointment" DROP CONSTRAINT "FK_907208f6adb9982267492cc3946"')
		await queryRunner.query('ALTER TABLE "appointment" DROP CONSTRAINT "UQ_907208f6adb9982267492cc3946"')
		await queryRunner.query('ALTER TABLE "appointment" DROP COLUMN "assistanceOfferId"')
	}
}
