module.exports = {
	displayName: 'Backend',
	globals: {
		'ts-jest': {
			tsConfig: '<rootDir>/tests/tsconfig.json',
			diagnostics: true
		}
	},
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
	preset: 'ts-jest',
	testMatch: ['<rootDir>/tests/**/*.spec.(js|jsx|ts|tsx)'],
	coverageReporters: [
		'json'
	],
	collectCoverageFrom: [
		'src/**/*.ts'
	]
}
