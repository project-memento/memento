module.exports = {
	extends: ['../.eslintrc.js'],
	env: {
		node: true
	},
	rules: {
		'no-console': 'off',
		'no-debugger': 'off',
		'no-template-curly-in-string': 'off',
		'no-useless-constructor': 'off'
	},
	overrides: [
		{
			files: ['migrations/**/*.ts'],
			rules: {
				'max-len': 'off'
			}
		}
	]
}
