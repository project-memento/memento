module.exports = function (wallaby) {
	return {
		files: [
			'jest.config.js',
			'tests/tsconfig.json',
			'src/**/*.ts',
			'tests/**/*.ts',
			'!tests/**/*.spec.ts'
		],

		tests: ['tests/**/*.spec.ts'],

		env: {
			type: 'node',
			runner: 'node',
			params: {
				env: 'NODE_ENV=test'
			}
		},

		compilers: {
			'**/*.ts': wallaby.compilers.typeScript()
		},

		testFramework: 'jest',
		workers: {
			initial: 2
		}
	}
}
