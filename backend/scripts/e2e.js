#!/usr/bin/env node

const path = require('path')

require('ts-node').register({
	project: path.resolve(__dirname, '../tsconfig.json'),
	transpileOnly: true
})
const { Env } = require('@tsed/core')
const getPort = require('get-port')
const { Server } = require('../src/Server')
const { createConnection } = require('typeorm')
const connection = {
	...require('../tests/ormconfig').connection
}

async function startBackend() {
	const backendServer = new Server()
	backendServer.settings.set({
		env: Env.TEST,
		httpPort: await getPort(),
		typeorm: [connection],
		'graphql.default.buildSchemaOptions.emitSchemaFile': false
	})
	await backendServer.start()
	return {
		url: `http://${backendServer.settings.httpPort}/`.replace('0.0.0.0', 'localhost'),
		server: backendServer
	}
}

module.exports = {
	default: startBackend,
	connection,
	typeorm() {
		return createConnection(connection)
	}
}
