import { ConnectionOptions } from 'typeorm'
import * as path from 'path'

const rootFolder = path.resolve(__dirname, '../')

export const connection: ConnectionOptions = {
	name: 'default',
	type: 'sqlite',
	database: ':memory:',
	synchronize: true,
	dropSchema: true,
	entities: [
		rootFolder + '/src/entities/**/*.{js,ts}'
	],
	cache: false
}
