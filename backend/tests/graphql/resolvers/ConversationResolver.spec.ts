import { inject, TestContext } from '@tsed/testing'
import { cleanAndApply } from '../../utils'
import { School } from '../../../src/entities/School'
import { RepositoryService } from '../../../src/services/RepositoryService'
import EntityFixtures from '../../../src/fixtures/EntityFixtures'
import { Conversation } from '../../../src/entities/Conversation'
import { ConversationResolver } from '../../../src/graphql/resolvers/ConversationResolver'
import { Message } from '../../../src/entities/Message'
import * as Faker from 'faker'
import { ConversationParticipant } from '../../../src/entities/ConversationParticipant'
import { ConversationParticipantResolver } from '../../../src/graphql/resolvers/ConversationParticipantResolver'

describe('graphql/resolvers/ConversationResolver', () => {
	beforeAll(TestContext.create)
	afterEach(TestContext.reset)

	let school: School
	beforeEach(cleanAndApply(async fixtureManager => {
		school = await fixtureManager.School()
	}))

	it('should only show conversations your apart of',
		inject([ConversationResolver, RepositoryService],
			async (resolver: ConversationResolver, repoService: RepositoryService) => {
				const fixtureManager = new EntityFixtures(repoService)
				const user1 = await fixtureManager.User({ school })
				const user2 = await fixtureManager.User({ school })
				const user3 = await fixtureManager.User({ school })
				const conversationRepo = repoService.getRepository(Conversation)

				const user1n2 = await conversationRepo.save(new Conversation([user1, user2]))
				const user1n3 = await conversationRepo.save(new Conversation([user1, user3]))
				const user2n3 = await conversationRepo.save(new Conversation([user2, user3]))

				const user1Conversations = await resolver.conversations({ user: user1 })
				await expect(user1Conversations).toContainEqual(expect.objectContaining({
					id: user1n2.id,
					participants: expect.arrayContaining([
						expect.objectContaining({
							user: expect.objectContaining({ id: user1.id })
						}),
						expect.objectContaining({
							user: expect.objectContaining({ id: user2.id })
						})
					])
				}))
				await expect(user1Conversations).toContainEqual(expect.objectContaining({ id: user1n3.id }))

				const user2Conversations = await resolver.conversations({ user: user2 })
				await expect(user2Conversations).toContainEqual(expect.objectContaining({ id: user1n2.id }))
				await expect(user2Conversations).toContainEqual(expect.objectContaining({ id: user2n3.id }))

				const user3Conversations = await resolver.conversations({ user: user3 })
				await expect(user3Conversations).toContainEqual(expect.objectContaining({ id: user1n3.id }))
				await expect(user3Conversations).toContainEqual(expect.objectContaining({ id: user2n3.id }))
			})
	)

	it('should update last read message',
		inject([ConversationResolver, ConversationParticipantResolver, RepositoryService],
			async (resolver: ConversationResolver, participantResolver: ConversationParticipantResolver, repoService: RepositoryService) => {
				const fixtureManager = new EntityFixtures(repoService)
				const user1 = await fixtureManager.User({ school })
				const user2 = await fixtureManager.User({ school })
				const conversationParticipantRepo = repoService.getRepository(ConversationParticipant)
				const conversationRepo = repoService.getRepository(Conversation)
				const messageRepo = repoService.getRepository(Message)

				let conversation = await conversationRepo.save(new Conversation([user1, user2]))

				await messageRepo.save(Array.apply(null, { length: 3 })
					.map(() => new Message(
						conversation,
						Faker.lorem.sentence(),
						Faker.random.arrayElement([user1, user2]),
						Faker.date.recent(Faker.random.number(5))
					)))

				conversation = await conversationRepo.findOne(conversation)

				let messages = [...await conversation.messages]

				messages.sort((a, b) => a.id > b.id ? -1 : 1)

				await expect(conversation.participants[0].lastReadMessage).toBeNull()
				await expect(conversation.participants[1].lastReadMessage).toBeNull()

				await expect(participantResolver.unreadMessages(conversation.participants[0]))
					.resolves.toEqual(3)

				await resolver.readMessage(messages[2].id, { user: user1 })

				let participant = await conversationParticipantRepo.findOne({ conversation, user: user1 })
				expect(participant).toEqual(expect.objectContaining({
					lastReadMessage: expect.objectContaining({
						id: messages[2].id
					})
				}))

				await expect(participantResolver.unreadMessages(participant))
					.resolves.toEqual(2)
				await resolver.readMessage(messages[0].id, { user: user1 })

				participant = await conversationParticipantRepo.findOne({ conversation, user: user1 })
				expect(participant).toEqual(expect.objectContaining({
					lastReadMessage: expect.objectContaining({
						id: messages[0].id
					})
				}))

				await expect(participantResolver.unreadMessages(participant))
					.resolves.toEqual(0)
			})
	)
})
