import { inject, TestContext } from '@tsed/testing'
import { RepositoryService } from '../../../src/services/RepositoryService'
import EntityFixtures from '../../../src/fixtures/EntityFixtures'
import { TestResolver } from '../../../src/graphql/resolvers/TestResolver'
import { School } from '../../../src/entities/School'
import { cleanAndApply } from '../../utils'

describe('graphql/resolvers/TestResolver', () => {
	beforeAll(TestContext.create)

	beforeEach(cleanAndApply(async fixtureManager => {}))

	afterEach(TestContext.reset)

	it('should apply serialized fixtures', inject(
		[TestResolver, RepositoryService],
		async (resolver: TestResolver, repoService: RepositoryService) => {
			const fixtures = new EntityFixtures()
			const response = await resolver.fixtures(JSON.stringify(await fixtures.School()))
			const deserializedSchool = await fixtures.deserialize(JSON.parse(response))
			const schoolRepo = repoService.getRepository(School)
			const dbSchool = await schoolRepo.findOne({
				relations: [
					'emailSuffixes',
					'campuses',
					'fieldsOfStudies',
					'students'
				]
			})

			expect(dbSchool.id).toEqual(deserializedSchool.id)
			expect((await dbSchool.emailSuffixes).map(({ suffix }) => suffix))
				.toEqual((await deserializedSchool.emailSuffixes).map(({ suffix }) => suffix).sort())
			expect((await dbSchool.campuses).map(({ id }) => id)).toEqual((await deserializedSchool.campuses).map(({ id }) => id).sort())
			expect((await dbSchool.fieldsOfStudies).map(({ id }) => id))
				.toEqual((await deserializedSchool.fieldsOfStudies).map(({ id }) => id).sort())
		}))

	it('should apply serialized fixtures', inject(
		[TestResolver, RepositoryService],
		async (resolver: TestResolver, repoService: RepositoryService) => {
			const fixtures = new EntityFixtures()
			const response = await resolver.fixtures(JSON.stringify(await fixtures.School()))
			const deserializedSchool = await fixtures.deserialize(JSON.parse(response))
			const schoolRepo = repoService.getRepository(School)
			const dbSchool = await schoolRepo.findOne({
				relations: [
					'emailSuffixes',
					'campuses',
					'fieldsOfStudies',
					'students'
				]
			})

			expect(dbSchool.id).toEqual(deserializedSchool.id)
			expect((await dbSchool.emailSuffixes).map(({ suffix }) => suffix))
				.toEqual((await deserializedSchool.emailSuffixes).map(({ suffix }) => suffix).sort())
			expect((await dbSchool.campuses).map(({ id }) => id)).toEqual((await deserializedSchool.campuses).map(({ id }) => id).sort())
			expect((await dbSchool.fieldsOfStudies).map(({ id }) => id))
				.toEqual((await deserializedSchool.fieldsOfStudies).map(({ id }) => id).sort())
		}))

	it('should clean db', inject(
		[TestResolver, RepositoryService],
		async (resolver: TestResolver, repoService: RepositoryService) => {
			const schoolRepo = repoService.getRepository(School)
			const fixtures = new EntityFixtures(repoService)
			await fixtures.School()
			await expect(schoolRepo.find()).resolves.toHaveLength(1)
			await resolver.cleanDB()
			await expect(schoolRepo.find()).resolves.toHaveLength(0)
		}))
})
