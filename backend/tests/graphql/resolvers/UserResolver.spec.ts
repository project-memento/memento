import 'reflect-metadata'
import { UserResolver } from '../../../src/graphql/resolvers/UserResolver'
import { inject, TestContext } from '@tsed/testing'
import { UserProfile } from '../../../src/entities/UserProfile'
import { TypeORMModule, TypeORMService } from '../../../src/extensions/typeorm'
import { ServerSettingsService } from '@tsed/common'
import { typeorm } from '../../../src/TypeOrm'
import { RepositoryService } from '../../../src/services/RepositoryService'
import { School } from '../../../src/entities/School'
import { BadRequest } from 'ts-httpexceptions'
import EntityFixtures from '../../../src/fixtures/EntityFixtures'
import { Tag } from '../../../src/entities/Tag'
import { StudentYear } from '../../../src/entities/StudentYear'
import { connection } from '../../ormconfig'
import { cleanAndApply } from '../../utils'

describe('graphql/resolvers/UserResolver', () => {
	let school: School
	let skill: Tag
	beforeAll(async () => {
		await TestContext.create()
	})

	beforeEach(cleanAndApply(async fixtureManager => {
		school = await fixtureManager.School()
		skill = await fixtureManager.Tag()
	}))

	afterEach(TestContext.reset)

	it('should create user with profile and emails', inject([UserResolver], async (resolver: UserResolver) => {
		const profile = new UserProfile()
		profile.firstname = 'John'
		profile.lastname = 'Doe'
		profile.imageLink = 'google.com/image.jpg'
		profile.currentYear = StudentYear.FIRST_YEAR

		const user = await resolver.createUser(
			'123456',
			'test@' + (await school.emailSuffixes)[0].suffix,
			profile,
			school.id,
			(await school.fieldsOfStudies)[0].id,
			[ skill.id ]
		)

		expect(user).not.toBeUndefined()
		expect(user.password).not.toEqual('123456')
		expect(user.emails).toHaveLength(1)
	}))

	it('should not create use if not valid email suffix', inject([UserResolver], async (resolver: UserResolver) => {
		const profile = new UserProfile()
		profile.firstname = 'John'
		profile.lastname = 'Doe'
		profile.imageLink = 'google.com/image.jpg'

		await expect(resolver.createUser(
			'123456',
			'test@invalid-' + (await school.emailSuffixes)[0].suffix,
			profile,
			school.id,
			(await school.fieldsOfStudies)[0].id,
			[ skill.id ]
		)).rejects.toThrowError(new BadRequest('Not a valid email-suffix'))
	}))
})
