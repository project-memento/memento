import { inject, TestContext } from '@tsed/testing'
import { cleanAndApply } from '../../utils'
import { User } from '../../../src/entities/User'
import { RepositoryService } from '../../../src/services/RepositoryService'
import { MessageResolver } from '../../../src/graphql/resolvers/MessageResolver'
import { Conversation } from '../../../src/entities/Conversation'
import { Message } from '../../../src/entities/Message'
import * as Faker from 'faker'

describe('graphql/resolvers/MessageResolver', () => {
	beforeAll(TestContext.create)
	afterEach(TestContext.reset)

	let user1: User
	let user2: User
	let user3: User
	let conversation: Conversation
	beforeEach(cleanAndApply(async (fixtureManager, repoService) => {
		const school = await fixtureManager.School()
		user1 = await fixtureManager.User({ school })
		user2 = await fixtureManager.User({ school })
		user3 = await fixtureManager.User({ school })

		conversation = await fixtureManager.save(new Conversation([user1, user2]))

		const messageRepo = repoService.getRepository(Message)
		await messageRepo.save(Array.apply(null, { length: 50 })
			.map(() => new Message(conversation, Faker.lorem.sentence(), Faker.random.arrayElement([user1, user2]), Faker.date.recent(3))))
	}))

	it('should get all messages from conversation in chunks of 25', inject(
		[MessageResolver, RepositoryService],
		async (resolver: MessageResolver, repoService: RepositoryService) => {
			const firstChunk = await resolver.messages({ user: user1 }, conversation.id, null)
			await expect(firstChunk.items).toHaveLength(25)
			expect(firstChunk.total).toEqual(50)
			expect(firstChunk.hasMore).toEqual(true)

			const lastChunk = await resolver.messages({ user: user1 }, conversation.id, firstChunk.items[firstChunk.items.length - 1].id)
			await expect(lastChunk.items).toHaveLength(25)
			expect(lastChunk.total).toEqual(50)
			expect(lastChunk.hasMore).toEqual(false)
			await expect(lastChunk.items).not.toEqual(expect.arrayContaining(firstChunk.items))

			const emptyChunk = await resolver.messages({ user: user1 }, conversation.id, lastChunk.items[lastChunk.items.length - 1].id)

			await expect(emptyChunk.items).toHaveLength(0)
			expect(emptyChunk.total).toEqual(50)
			expect(emptyChunk.hasMore).toEqual(false)
			const combined = [...firstChunk.items, ...lastChunk.items]
			const definitelySorted = [...combined].sort((a, b) => a.id > b.id ? -1 : 1)
			expect(combined).toEqual(definitelySorted)
		})
	)

	it('should send message to conversation', inject(
		[MessageResolver, RepositoryService],
		async (resolver: MessageResolver, repoService: RepositoryService) => {
			const msgRepo = repoService.getRepository(Message)
			const before = await resolver.messages({ user: user1 }, conversation.id, null)
			expect(before.total).toEqual(50)
			const publisher = jest.fn()

			const firstMessage = await resolver.sendMessage({ user: user1 }, conversation.id, 'First message', publisher)
			expect(firstMessage.message).toEqual('First message')
			expect(publisher).toHaveBeenCalledWith(firstMessage)

			const after = await resolver.messages({ user: user1 }, conversation.id, null)
			expect(after.total).toEqual(51)
			await expect(after.items[0])
				.toEqual(expect.objectContaining({
					id: firstMessage.id,
					message: firstMessage.message,
					sentAt: firstMessage.sentAt
				}))
		})
	)

	it('should not be able to send message to conversation the user is not apart of', inject(
		[MessageResolver],
		async (resolver: MessageResolver) => {
			const publisher = jest.fn()
			await expect(resolver.sendMessage({ user: user1 }, 'jijsdifjisjdfuisfu', 'Something', publisher))
				.rejects.toHaveProperty('message', 'Unknown conversationId')
			expect(publisher).not.toHaveBeenCalled()

			await expect(resolver.sendMessage({ user: user3 }, conversation.id, 'Something', publisher))
				.rejects.toHaveProperty('message', 'Unknown conversationId')
			expect(publisher).not.toHaveBeenCalled()
		}
	))

	it('should not be able to get messages from conversations the user is not apart of', inject(
		[MessageResolver],
		async (resolver: MessageResolver) => {
			await expect(resolver.messages({ user: user3 }, conversation.id, null))
				.rejects.toHaveProperty('message', 'Unknown conversationId')

			await expect(resolver.messages({ user: user1 }, 'invalid-id', null))
				.rejects.toHaveProperty('message', 'Unknown conversationId')
		})
	)
})
