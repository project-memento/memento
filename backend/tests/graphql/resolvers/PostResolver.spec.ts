import 'reflect-metadata'
import { TestContext, inject } from '@tsed/testing'
import { User } from '../../../src/entities/User'
import { Category } from '../../../src/entities/Category'
import { PostResolver } from '../../../src/graphql/resolvers/PostResolver'
import { cleanAndApply } from '../../utils'
import { Tag } from '../../../src/entities/Tag'
import EntityFixtures from '../../../src/fixtures/EntityFixtures'
import { RepositoryService } from '../../../src/services/RepositoryService'
import { Post } from '../../../src/entities/Post'

describe('graphql/resolvers/PostResolver', () => {
	beforeAll(TestContext.create)
	afterEach(TestContext.reset)

	let user1: User
	let user2: User
	let user3: User
	let category1: Category
	let category2: Category
	let tag1: Tag
	let tag2: Tag
	let tag3: Tag
	let tag4: Tag
	beforeEach(cleanAndApply(async fixtureManager => {
		const school = await fixtureManager.School()
		user1 = await fixtureManager.User({ school })
		user2 = await fixtureManager.User({ school })
		user3 = await fixtureManager.User({ school })
		category1 = await fixtureManager.Category()
		category2 = await fixtureManager.Category()
		tag1 = await fixtureManager.Tag()
		tag2 = await fixtureManager.Tag()
		tag3 = await fixtureManager.Tag()
		tag4 = await fixtureManager.Tag()
	}))

	it('should create post', inject([PostResolver], async (resolver: PostResolver) => {
		const ctx = { user: { user1 } }
		const date = new Date()
		const publisher = jest.fn()
		const post = await resolver.createPost(
			'Test title',
			'test content',
			date,
			'test location',
			'',
			category1.id,
			[tag1.id, tag2.id],
			publisher,
			ctx
		)

		expect(post).not.toBeUndefined()
		expect(post.title).toEqual('Test title')
		expect(post.content).toEqual('test content')
		expect(post.deadline).toEqual(date)
		expect(post.category).not.toBeUndefined()
		expect(post.tags.length).toEqual(2)
	}))

	describe('posts', () => {
		beforeEach(inject([RepositoryService], async (repoService: RepositoryService) => {
			const fixtureManager = new EntityFixtures(repoService)
			for (let i = 0; i < 1; i++) {
				await fixtureManager.Post({ user: user1, tags: [tag1, tag2], category: category1 })
			}
			for (let i = 0; i < 2; i++) {
				await fixtureManager.Post({ user: user2, tags: [tag1, tag3], category: category2 })
			}
			for (let i = 0; i < 3; i++) {
				await fixtureManager.Post({ user: user3, tags: [tag2, tag4], category: category1 })
			}
		}))

		it('should find only users posts', inject([PostResolver], async (resolver: PostResolver) => {
			const ctx = { user: user1 }
			const posts = await resolver.posts(
				[],
				[],
				[],
				true,
				true,
				null,
				ctx
			)
			expect(posts.length).toEqual(1)
		}))

		it('should not include own posts', inject([PostResolver], async (resolver: PostResolver) => {
			const ctx = { user: user1 }
			const posts = await resolver.posts(
				[],
				[],
				[],
				false,
				true,
				null,
				ctx
			)
			expect(posts.length).toEqual(5)
		}))

		it.skip('should find posts with tagIds', inject([PostResolver], async (resolver: PostResolver) => {
			const ctx = { user: user1 }
			let posts = await resolver.posts(
				[],
				[],
				[tag4.id],
				false,
				true,
				null,
				ctx
			)
			expect(posts.length).toEqual(3)

			posts = await resolver.posts(
				[],
				[tag4.id],
				[],
				false,
				true,
				null,
				ctx
			)
			expect(posts.length).toEqual(0)
		}))
	})
})
