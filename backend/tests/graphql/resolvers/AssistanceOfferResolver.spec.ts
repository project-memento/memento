import { inject, TestContext } from '@tsed/testing'
import { RepositoryService } from '../../../src/services/RepositoryService'
import { AssistanceOfferResolver } from '../../../src/graphql/resolvers/AssistanceOfferResolver'
import { cleanAndApply } from '../../utils'

describe('graphql/resolvers/AssistanceOfferResolver', () => {
	beforeAll(TestContext.create)

	let user
	let category
	let post
	beforeEach(cleanAndApply(async fixtureManager => {
		user = await fixtureManager.User()
		category = await fixtureManager.Category()
		post = await fixtureManager.Post({ user: user, category: category })
	}))

	afterEach(TestContext.reset)

	it.skip('should create assistance offers', inject(
		[AssistanceOfferResolver, RepositoryService],
		async (resolver: AssistanceOfferResolver, repoService: RepositoryService) => {
			console.log(post)
			// const response = resolver.createAssistanceOffer(post.user.id, post.id, 'Some description')
			// console.log(response)
		}))
})
