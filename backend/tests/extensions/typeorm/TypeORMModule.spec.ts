import { inject, TestContext } from '@tsed/testing'
import { TypeORMModule } from '../../../src/extensions/typeorm'
import { InjectorService, ServerSettingsService } from '@tsed/common'

describe('extensions/typeorm/TypeORMModule.ts', () => {
	const connection = {
		close() {
		}
	}
	let createConnectionStub
	beforeEach(
		inject([TypeORMModule, ServerSettingsService], async (service: TypeORMModule, settings: ServerSettingsService) => {
			this.service = service
			settings.set('typeorm', {
				db1: {
					config: 'config'
				}
			})

			// @ts-ignore
			createConnectionStub = jest.spyOn(this.service.typeORMService, 'createConnection')

			// @ts-ignore
			createConnectionStub.mockResolvedValue(connection)
		})
	)

	afterEach(inject([ServerSettingsService], async (settings: ServerSettingsService) => {
		settings.set('typeorm', {})
		await TestContext.reset()
	}))

	it('should connect on $onInit',
		inject([TypeORMModule, InjectorService], async (service: TypeORMModule, injector: InjectorService) => {
			jest.spyOn(injector, 'toArray').mockReturnValue([])

			const result = await service.$onInit()
			expect(createConnectionStub)
				.toHaveBeenNthCalledWith(1, 'db1', { config: 'config' })
			expect(result).toEqual([ connection ])
		}))

	it('should emit $afterDBInit', inject(
		[
			TypeORMModule,
			ServerSettingsService,
			InjectorService
		],
		async (typeorm: TypeORMModule, settings: ServerSettingsService, injector: InjectorService) => {
			const mock = jest.fn()

			const service = {
				$afterDBInit: mock
			}
			// @ts-ignore
			jest.spyOn(injector, 'toArray').mockReturnValue([service])

			await typeorm.$onInit()

			expect(mock).toHaveBeenCalled()
		}))
})
