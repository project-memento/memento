import * as TypeORM from 'typeorm'
import { TestContext } from '@tsed/testing'
import { TypeORMService } from '../../../../src/extensions/typeorm'

describe('extensions/typeorm/services/TypeORMService.ts', () => {
	it('should create and close connection', async () => {
		const connection = {
			close: jest.fn()
		}

		// @ts-ignore
		const spy = jest.spyOn(TypeORM, 'createConnection').mockResolvedValue(connection)

		const service = new TypeORMService()

		const result1 = await service.createConnection('key', { config: 'config' } as any)
		const result2 = await service.createConnection('key', { config: 'config' } as any)

		expect(result1).toEqual(connection)
		expect(result2).toEqual(connection)
		expect(spy).toHaveBeenNthCalledWith(1, { config: 'config' })

		await service.closeConnections()

		expect(connection.close).toBeCalled()
	})
})
