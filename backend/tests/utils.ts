import EntityFixtures from '../src/fixtures/EntityFixtures'
import { inject } from '@tsed/testing'
import { TypeORMModule, TypeORMService } from '../src/extensions/typeorm'
import { RepositoryService } from '../src/services/RepositoryService'
import { ServerSettingsService } from '@tsed/common'
import { connection } from './ormconfig'

export function cleanAndApply(fn: (
	(fixtureManager: EntityFixtures) => void | Promise<any>) |
	((fixtureManager: EntityFixtures, repoService : RepositoryService) => void | Promise<any>)) {
	return inject(
		[
			TypeORMService,
			RepositoryService,
			TypeORMModule,
			ServerSettingsService
		],
		async (typeorm: TypeORMService, repoService: RepositoryService, service: TypeORMModule, settings: ServerSettingsService) => {
			settings.set('typeorm', [connection])
			await service.$onInit()
			await typeorm.get().synchronize(true)
			return fn(new EntityFixtures(repoService), repoService)
		}
	)
}
