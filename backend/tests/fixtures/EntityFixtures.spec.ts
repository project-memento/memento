import EntityFixtures from '../../src/fixtures/EntityFixtures'
import { School } from '../../src/entities/School'
import { Campus } from '../../src/entities/Campus'

describe('EntityFixtures', () => {
	it('should serialize and deserialize seeds', async () => {
		const entityFixtures = new EntityFixtures()
		const school = await entityFixtures.School()
		await expect(school).not.toBeInstanceOf(School)
		const deSchool = await entityFixtures.deserialize(school)
		await expect(deSchool).toBeInstanceOf(School)
		await expect(deSchool.campuses[0]).toBeInstanceOf(Campus)
	})
})
