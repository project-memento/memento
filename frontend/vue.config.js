// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const path = require('path')
const PrerenderSPAPlugin = require('prerender-spa-plugin')
const webpack = require('webpack')

module.exports = {
	outputDir: '../public',
	css: {
		loaderOptions: {
			sass: {
				data: '@import "@/styles/variables.sass"'
			}
		}
	},
	chainWebpack: config => {
		config.plugin('type-graphql')
			.use(webpack.NormalModuleReplacementPlugin, [
				/type-graphql$/,
				resource => {
					resource.request = resource.request.replace(/type-graphql/, 'type-graphql/browser-shim')
				}
			])
		if (process.env.NODE_ENV === 'production') {
			config.plugin('prerender')
				.use(PrerenderSPAPlugin, [{
					staticDir: path.join(__dirname, '../public'),
					routes: ['/']
				}])
		}

		const svgRule = config.module.rule('svg')
		svgRule.uses.clear()
		svgRule
			.use('vue-svg-loader')
			.loader('vue-svg-loader')
			.options({
				svgo: {
					plugins: [
						{ inlineStyles: true },
					],
				},
			})
	},
	configureWebpack: {
		plugins: [
			// new BundleAnalyzerPlugin(),
			new webpack.IgnorePlugin({
				resourceRegExp: /class-validator/
			})
		]
	},
	devServer: {
		proxy: {
			'/graphql': {
				target: 'http://localhost:8081/',
				changeOrigin: true,
				ws: true
			},
			'/uploads': {
				target: 'http://localhost:8081/',
				changeOrigin: true
			},
			'/api/v1/upload': {
				target: 'http://localhost:8081/',
				changeOrigin: true
			},
		}
	}
}
