import '@babel/polyfill'
import 'object.fromentries/auto'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './plugins/i18n'
import { apolloProvider } from './vue-apollo'
import './styles/global.sass'
import { ApiService } from '@/services/api.service'
import { TokenService } from '@/services/storage.service'
require('typeface-roboto')
ApiService.init()
if (TokenService.token) {
	ApiService.setHeader()
	ApiService.mount401Interceptor()
}

Vue.config.productionTip = false

const app = new Vue({
	router,
	store,
	i18n,
	apolloProvider,
	render: h => h(App)
}).$mount('#app')

// If running e2e tests...
if (process.env.VUE_APP_TEST === 'e2e') {
	// Attach the app to the window, which can be useful
	// for manually setting state in Cypress commands
	// such as `cy.logIn()`.
	// @ts-ignore
	window.__app__ = app
}
