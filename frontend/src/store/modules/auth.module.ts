import { TokenService } from '@/services/storage.service'
import { AuthenticationError, UserService } from '@/services/user.service'
import router from '@/router'
import { VuexModule, Module, mutation, action } from 'vuex-class-component'
import { oc } from 'ts-optchain'
import { vxm } from '@/store'

@Module({ namespacedPath: 'auth/' })
export class AuthModule extends VuexModule {
	authenticating = false
	accessToken = TokenService.token
	authenticationErrorCode = 0
	authenticationError = ''
	refreshTokenPromise: Promise<any> | null = null

	get loggedIn() {
		return !!this.accessToken
	}

	get decodedJWT() {
		if (this.accessToken) {
			return JSON.parse(atob(this.accessToken.split('.')[1]))
		}
		return null
	}

	get userId() {
		if (this.accessToken) {
			return JSON.parse(atob(this.accessToken.split('.')[1]))['sub']
		}
		return null
	}

	@mutation
	loginRequest() {
		this.authenticating = true
		this.authenticationError = ''
		this.authenticationErrorCode = 0
	}

	@mutation
	loginSuccess(accessToken: string) {
		this.accessToken = accessToken
		this.authenticating = false
	}

	@mutation
	loginError({ errorCode, error }: { errorCode: number, error: string }) {
		this.authenticating = false
		this.authenticationError = error
		this.authenticationErrorCode = errorCode
	}

	@mutation
	logoutSuccess() {
		this.accessToken = null
		this.refreshTokenPromise = null
	}

	@mutation
	setRefreshTokenPromise(promise: Promise<any> | null) {
		this.refreshTokenPromise = promise
	}

	@action()
	async logout() {
		await UserService.logout()
		this.logoutSuccess()
		router.push('/')
	}

	@action()
	async login({ email, password }: { email: string, password: string }): Promise<boolean> {
		this.loginRequest()

		try {
			const token = await UserService.login(email, password)
			this.loginSuccess(token)
			return true
		} catch (e) {
			this.loginError({ errorCode: e.errorCode, error: e.message })
			return false
		}
	}

	@action
	async refreshToken() {
		if (!this.refreshTokenPromise) {
			const promise = UserService.refreshToken()

			this.setRefreshTokenPromise(promise)

			try {
				const token = await promise

				this.loginSuccess(token)
			} finally {
				this.setRefreshTokenPromise(null)
			}
		}

		return this.refreshTokenPromise
	}
}

export const auth = AuthModule.ExtractVuexModule(AuthModule)
