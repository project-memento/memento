import { VuexModule, Module, action, mutation } from 'vuex-class-component'
import { AuthModule } from '@/store/modules/auth.module'
import { apolloClient } from '@/vue-apollo'
import gql from 'graphql-tag'
import { UserService } from '@/services/user.service'

@Module({ namespacedPath: 'user/' })
export class UserModule extends VuexModule {
	me: any = null

	@mutation
	updateLoggedInUser(user) {
		this.me = user
	}

	@action
	async fetchLoggedInUser() {
		const response = await apolloClient.query({
			fetchPolicy: 'no-cache',
			query: gql`{
				me {
					id
					role
					profile {
						id
						firstname
						lastname
						imageLink
						currentYear
					}
					fieldOfStudy {
						id
						name
					}
					school {
						id
						name
						campuses {
							id
							name
						}
					}
					campuses {
						id
						name
					}
					skills {
						id
						tag
					}
				}
			}`
		})

		if (response.data.me == null) {
			return UserService.logout()
		}

		this.updateLoggedInUser(response.data.me)
	}

	@action()
	canAccess(roles: string[]) {
		return roles.includes(this.me.role)
	}

	@action
	async reset() {
		this.updateLoggedInUser(null)
	}
}

export const user = UserModule.ExtractVuexModule(UserModule)
