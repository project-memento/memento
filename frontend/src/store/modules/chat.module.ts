import { VuexModule, Module, action, mutation } from 'vuex-class-component'
import { Conversation } from '../../../../backend/src/entities/Conversation'
import { apolloClient } from '@/vue-apollo'
import gql from 'graphql-tag'
import { SubscribeToMessages } from '@/graphql/queries'
import { Message } from '../../../../backend/src/entities/Message'
import { Observable } from 'apollo-client/util/Observable'
import { vxm } from '@/store'
import { VueApollo } from 'vue-apollo/types/vue-apollo'

@Module({ namespacedPath: 'chat/' })
export class ChatModule extends VuexModule {
	conversations: Conversation[] = []
	observable: Observable<{ data: { messages: Message } }>|null = null
	subscription: ZenObservable.Subscription|null = null

	@mutation
	setSubscription(subscription: ZenObservable.Subscription|null) {
		this.subscription = subscription
	}

	@mutation
	setObservable(observable: Observable<{ data: { messages: Message } }>|null) {
		this.observable = observable
	}

	@mutation
	setConversations(conversations: Conversation[]) {
		this.conversations = conversations.map(conversation => ({
			...conversation,
			// @ts-ignore
			unreadMessages: conversation.participants.find(({ user }) => user.id === vxm.auth.userId)
				.unreadMessages
		}))
	}

	@mutation
	markConversationAsRead(conversationId: string) {
		const conversation = this.conversations
			.find(c => c.id === conversationId)
		if (conversation) {
			// @ts-ignore
			conversation.unreadMessages = 0
		}
	}

	@mutation
	updateLastMessage(message: Message) {
		const conversation = this.conversations
			.find(c => c.id === message.conversation.id)
		if (conversation) {
			// @ts-ignore
			conversation.unreadMessages = message.author.id === vxm.auth.userId
				? 0
			// @ts-ignore
				: conversation.unreadMessages + 1
			conversation.lastMessage = message
		}
	}

	@action
	async fetchConversations() {
		const result = await apolloClient.query<{ conversations: Conversation[] }>({
			query: gql`query {
				conversations {
					id
					lastMessage {
						author {
							profile {
								firstname
								lastname
							}
						}
						message
					}
					participants {
						user {
							id
							profile {
								firstname
								lastname
								imageLink
							}
						}
						lastReadMessage {
							id
						}
						unreadMessages
					}
				}
			}`
		})

		this.setConversations(result.data.conversations)
		this.subscribeForMessages()
	}

	@action
	async subscribeForMessages() {
		if (!this.isSubscribing) {
			const observable = apolloClient.subscribe({
				query: SubscribeToMessages
			})

			this.setObservable(observable)

			const subscription = observable.subscribe(async ({ data }) => {
				if (!this.conversations.some(({ id }) => data.messages.conversation.id)) {
					await this.fetchConversations()
				}
				this.updateLastMessage(data.messages)
			})

			this.setSubscription(subscription)
		}
	}

	@action
	async markAsRead(message: Message) {
		await apolloClient.mutate({
			mutation: gql`mutation($messageId: String!) {
				readMessage(messageId: $messageId) {
					id
				}
			}`,
			variables: {
				messageId: message.id
			}
		})

		this.markConversationAsRead(message.conversation.id)
	}

	get isSubscribing() {
		if (!this.subscription) {
			return false
		}
		return !this.subscription.closed
	}

	get unreadMessages() {
		// @ts-ignore
		return this.conversations.map(conversation => conversation.unreadMessages)
			.reduce((sum, count) => sum + count, 0)
	}
}

export const chat = ChatModule.ExtractVuexModule(ChatModule)
