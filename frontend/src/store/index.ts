import Vue from 'vue'
import Vuex from 'vuex'
import { auth, AuthModule } from '@/store/modules/auth.module'
import { user, UserModule } from '@/store/modules/user.module'
import { ChatModule, chat } from '@/store/modules/chat.module'

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		auth,
		user,
		chat
	}
})

export default store

const _resetVxm = () => ({
	auth: AuthModule.CreateProxy(store, AuthModule),
	user: UserModule.CreateProxy(store, UserModule),
	chat: ChatModule.CreateProxy(store, ChatModule)
})

export const vxm = _resetVxm()

export const resetVxm = () => {
	Object.entries(_resetVxm()).forEach(([name, proxy]) => {
		vxm[name] = proxy
	})
}
