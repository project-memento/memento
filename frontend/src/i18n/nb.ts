export default {
	'Category': 'Kategori',
	'Meeting point': 'Møtested',
	'Create': 'Opprett',
	'Upload image': 'Last opp bilde',
	'add_campus': 'Legg til campus',
	'name_common': 'Navn',
	'send_first_message': 'Send en første melding',
	rules: {
		required: {
			school: 'Du trenger å oppgi skolen',
			studyName: 'Du trenger å oppgi studielinje',
			studyYear: 'Du trenger å oppgi studieår'
		},
		validate: {
			studyYear: 'Du trenger å oppgi et gyldig studieår',
			invalid_credentials: 'Feil e-post eller passord'
		}
	},
	'register_with_email': 'Registrer deg med e-postadressen som tilhører din utdanningsinstitusjon',
	'will_be_visible_on_profile': 'Dette blir synlig for andre brukere og kan endres på din profil',
	'information_personal': 'Informasjon om behandling av personopplysninger',
	'upload_picture_text': 'Gjør det lettere for andre å knytte kontakt med deg ved å laste opp et bilde av deg selv',
	'educational_institution': 'Studiested',
	'field_of_study': 'Studielinje',
	'study_year': 'Studieår',
	'edit-school': 'Rediger skole',
	'next_step': 'Neste steg',
	'register': 'Registrer',
	'register_on_connect': 'Registrer deg på Connect',
	'privacy_policy': 'Personvernerklæring',
	'n_assistance_offers': '{0} nye forespørsler',
	'assistance_offer_from': 'Forespørsel fra {name}',
	'no_assistance_offers': 'Ingen forespørsler enda',
	'first_name': 'Fornavn',
	'last_name': 'Etternavn',
	'email': 'E-post',
	'password': 'Passord',
	'repeat_password': 'Gjenta passord',
	'privacy_policy_first_paragraph':
		'Vi i Connect samler inn og vil bruke dine personopplysninger, det vil si data som kan knyttes til våre brukere. ' +
		'Vi har ansvar for å håndtere data på en sikker og brukervennlig måte, som oppfyller både lovkrav og de forventninger brukerne våre har. ' +
		'Her kan du både lese om hvordan Connect behandler personopplysninger og dine rettigheter.',
	'privacy_policy_second_paragraph':
		'Det er Connect AS som er behandlingsansvarlig, ' +
		'det vil si den virksomheten som er ansvarlig for behandlingen av personopplysningene ' +
		'som er beskrevet i denne erklæringen.',
	'privacy_policy_third_paragraph':
		'Dersom du har en profil hos Connect, er dette selskapet behandlingsansvarlig for personopplysninger i ' +
		'forbindelse med administrasjon av din profil. ' +
		'Informasjonen om dine rettigheter og hvordan vi i Connect håndterer personopplysninger som du finner i denne teksten, ' +
		'gjelder også for Connect. ' +
		'Det betyr at du kan henvende deg til Connect AS sentralt uansett hvilket selskap henvendelsen din gjelder. ' +
		'I denne erklæringen finner du også informasjon om bruk av informasjonskapsler ("cookies") og lokal lagring på nettsidene til Connect.',
	'have_read_terms_and_conditions': 'Jeg har lest og forstått personvernerklæringen og bruk av informasjonskapsler',
	'accept_privacy_statement': 'Du må godta personvernerklæringen for å kunne gå videre',
	'chat_name': 'Navn',
	'chat_username': 'Brukernavn',
	'campus_name': 'Navn',
	'campus_address': 'Adresse',
	'create_school': 'Opprett skole',
	'new_school': 'Ny skole',
	'create_email_domain': 'Legg til nytt e-post domene',
	'new_email_domain': 'Nytt e-post domene',
	'email_name': 'Navn',
	'add_field_of_study': 'Legg til studielinje',
	'new_field_of_study': 'Ny studielinje',
	'field_of_study_name': 'Navn',
	'school_name': 'Navn',
	'appointments': 'Avtaler',
	'create_post': 'Opprett innlegg',
	'exit_popup': 'Vil du avslutte?',
	'exit_popup_text_dialog': 'Hvis du lukker vinduet uten å opprette et innlegg, vil dataen som er fylt inn gå tapt.',
	'disagree': 'Uenig',
	'agree': 'Enig',
	'new_post': 'Nytt innlegg',
	'sort': 'Sorter',
	'deadline': 'Frist',
	'category': 'Kategori',
	'place': 'Sted',
	'location': 'Sted',
	'desired_skills': 'Ønskede ferdigheter',
	'offer_sent': 'Forespørsel sendt',
	'offer_accepted':
		'{name} has received your request, and you will receive a notification when it has been accepted. ' +
		'Takk for at du vil hjelpe til!',
	'reset': 'Nullstill',
	'search': 'Søk',
	'profile': 'Profil',
	'cancel': 'Avbryt',
	'thanks_for_offer': 'Så flott at du ønsker å hjelpe til!',
	'offer_help': 'Tilby til hjelp',
	'help': 'Hjelp til',
	'i_can_help_you': 'Hei, jeg kan hjelpe deg...',
	'skills': 'Ferdigheter',
	'skills_hint': '"Oppgi hva du har lyst til å bidra med for kunne å se innlegg tilpasset til dine ferdigheter. ' +
		'For eks: &quot;Programmering&quot;, &quot;Grafisk design&quot; osv. "',
	'loading_messages': 'Laster inn meldinger',
	'new_ways_to_connect': 'Den nye måten å knytte kontakter',
	'user_professional_skills': 'Bruk din faglige kompetanse til å hjelpe dine medstudenter,' +
		'samtidig som du knytter nye kontakter.',
	'register_yourself': 'Registrer deg',
	'log_in': 'Logg inn',
	'log_out': 'Logg ut',
	'a_social_platform': 'En ny sosial tjeneste',
	'a_helping_platform': 'En tjeneste som skal hjelpe studenter med å opprette nye kontakter' +
		' på tvers av studielinjer og årskull. ' +
		'Connect er en tjeneste for alle studenter i hele Norge, hvor vi håper du vil knytte mange Connections sammen med oss!',
	'join': 'Join',
	'create_connections': 'Skap Connections',
	'ask_for_help': 'Be om hjelp',
	'need_help_with_something': 'Har du noe du trenger hjelp med?',
	'ask_for_fellow_students': 'Spør dine medstudenter!',
	'connect': 'Knytt kontakt',
	'get_to_know_your_contacts': 'Bli kjent med dine nye kontakter!',
	'agree_meeting': 'Avtal et møte',
	'meet_each_other': 'Møt hverandre og løs problemener du trengte hjelp med sammen!',
	'educational_place': 'Utdanningssted',
	'professional_skills': 'Faglige ferdigheter',
	'create_contacts': 'Knytt nye kontakter',
	'join_and_create': 'Bli med og skap Connections med andre studenter på din skole!',
	'dont_have_user': 'Har du ikke bruker?',
	'forgot_password': 'Jeg har glemt passordet mitt',
	'do_you_have_a_user': 'Har du allerede en bruker?',
	studyYear: {
		'FIRST_YEAR': 'Bachelor Første studieår',
		'SECOND_YEAR': 'Bachelor Andre studieår',
		'THIRD_YEAR': 'Bachelor Tredje studieår',
		'FIRST_MASTER': 'Mastergrad første år',
		'SECOND_MASTER': 'Mastergrad andre år',
		short: {
			'FIRST_YEAR': 'Bachelor, 1. år',
			'SECOND_YEAR': 'Bachelor, 2. år',
			'THIRD_YEAR': 'Bachelor, 4. år',
			'FIRST_MASTER': 'Master, 1. år',
			'SECOND_MASTER': 'Master, 2. år',
		}
	}
}
