export default {
	rules: {
		required: {
			generic: 'This field is required',
			school: 'You have to provide a school',
			studyName: 'You have to provide a field of study',
			studyYear: 'You have to provide your grade year',
			termsAndConditions: 'You have to accept the terms'
		},
		validate: {
			studyYear: 'You have to specify a valid StudentYear',
			email: 'Please enter a valid email',
			invalidEmailSuffix: 'Could not find any school with that email suffix',
			password: 'Must contain 8 characters, numbers, and different capitalization',
			confirmPassword: 'The password must match',
			invalid_credentials: 'Wrong e-mail or password'
		}
	},
	'privacy_policy': 'Privacy policy',
	'register_on_connect': 'Register on Connect',
	'register_with_email': 'Register with the e-mail address of your educational institution',
	'will_be_visible_on_profile': 'This will be visible to other users and can be changed on your profile',
	'information_personal': 'Information about the processing of personal data',
	'upload_picture_text': 'Make it easier for others to connect with you by uploading a photo of yourself',
	'n_assistance_offers': '{0} new offers',
	'assistance_offer_from': 'Offer from {name}',
	'name_common': 'Name',
	'educational_institution': 'Institution',
	'field_of_study': 'Field of study',
	'study_year': 'Study year',
	'edit_school': 'Edit school',
	'next_step': 'Next step',
	'register': 'Register',
	'Category': 'Category',
	'Meeting point': 'Meeting point',
	'Create': 'Create',
	'add_campus': 'Add campus',
	'first_name': 'First name',
	'last_name': 'Last name',
	'email': 'E-mail',
	'password': 'Password',
	'repeat_password': 'Repeat password',
	'privacy_policy_first_paragraph':
		'We at Connect collect and use personal information, that is, data that can be linked to our users. ' +
		'We are responsible for managing data in a safe and user-friendly manner that meets both legal requirements ' +
		'and the expectations of our users. ' +
		'Here you can read both about how Connect processes personal information and your rights.',
	'privacy_policy_second_paragraph':
		'It is Connect AS who is the data controller,' +
		'That is, the business that is responsible for processing the personal information' +
		'as described in this statement.',
	'privacy_policy_third_paragraph':
		'If you have a profile at Connect, this company is the person responsible for processing personal information in' +
		'connection with administration of your profile. ' +
		'The information about your rights and how we at Connect handle the personal information you find in this text,' +
		'also applies to Connect. ' +
		'That means you can contact Connect AS centrally regardless of which company your inquiry applies to. ' +
		'In this statement you will also find information on the use of cookies ("cookies") and local storage on the Connect website. ',
	'have_read_terms_and_conditions': 'I have read and understood the privacy statement and the use of cookies',
	'accept_privacy_statement': 'You must accept the privacy statement in order to proceed',
	'chat_name': 'Name',
	'chat_username': 'Username',
	'campus_name': 'Name',
	'campus_address': 'Address',
	'create_school': 'Create school',
	'new_school': 'New school',
	'create_email_domain': 'Create email domain',
	'new_email_domain': 'New email domain',
	'email_name': 'Name',
	'add_field_of_study': 'Add field of study',
	'new_field_of_study': 'New field of study',
	'field_of_study_name': 'Name',
	'school_name': 'Name',
	'appointments': 'Appointments',
	'create_post': 'Create post',
	'exit_popup': 'Do you want to exit',
	'exit_popup_text_dialog': 'If you exit, all your unsaved data will be deleted',
	'disagree': 'Disagree',
	'agree': 'Agree',
	'new_post': 'New post',
	'sort': 'Sort',
	'deadline': 'Deadline',
	'category': 'Category',
	'location': 'Location',
	'desired_skills': 'Preferred skills',
	'offer_accepted':
		'{name} has received your request, and you will receive a notification when it has been accepted.' +
		' Thanks for helping!',
	'offer_sent': 'Request sent',
	'place': 'Place',
	'reset': 'Reset',
	'search': 'Search',
	'log_out': 'Log out',
	'profile': 'Profile',
	'cancel': 'Cancel',
	'thanks_for_offer': 'So great that you want to help!',
	'offer_help': 'Offer help',
	'help': 'Help',
	'i_can_help_you': 'Hi, I can help you...',
	'skills': 'Skills',
	'skills_hint': 'Enter what you would like to contribute to see posts tailored to your skills. For example: "' +
		'Programming", "Graphic Design" etc.',
	'new_ways_to_connect': 'The new way to connect',
	'loading_messages': 'Loading messages',
	'user_professional_skills': 'Use your professional skills to help your fellow students,\n' +
		'while making new contacts.',
	'register_yourself': 'Register',
	'log_in': 'Login',
	'a_social_platform': 'A new social platform',
	'a_helping_platform': 'A service that will help students connect contacts across student lines and cohorts. ' +
		'Connect is a service for all students throughout Norway, and hope you will connect many Connections with us!',
	'join': 'Join',
	'create_connections': 'Create connections',
	'ask_for_help': 'Ask for help',
	'need_help_with_something': 'Do you have anything you need help with?',
	'ask_for_fellow_students': 'Ask your fellow students!',
	'connect': 'Connect',
	'get_to_know_your_contacts': 'Get to know your new contacts!',
	'agree_meeting': 'Agree on a meeting',
	'meet_each_other': 'Meet each other and find out the problems you needed help with, together!',
	'educational_place': 'Educational place',
	'professional_skills': 'professional skills',
	'create_contacts': 'create new contacts',
	'join_and_create': 'Join and create Connections with other students at your school!',
	'dont_have_user': 'Dont have a user?',
	'forgot_password': 'I forgot my password',
	'do_you_have_a_user': 'Already have a user?',
	studyYear: {
		'FIRST_YEAR': 'Bachelor, Første studieår',
		'SECOND_YEAR': 'Bachelor, Andre studieår',
		'THIRD_YEAR': 'Bachelor, Tredje studieår',
		'FIRST_MASTER': 'Mastergrad, første år',
		'SECOND_MASTER': 'Mastergrad, andre år',
		short: {
			'FIRST_YEAR': 'Bachelor, 1. year',
			'SECOND_YEAR': 'Bachelor, 2. year',
			'THIRD_YEAR': 'Bachelor, 4. year',
			'FIRST_MASTER': 'Master, 1. year',
			'SECOND_MASTER': 'Master, 2. year',
		}
	}
}
