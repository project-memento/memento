import { debounce } from 'lodash'
import { DebounceOptions } from 'lodash-decorators/shared'

export function Debounce(seconds: number, options: DebounceOptions = {}): MethodDecorator {
	return <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) : TypedPropertyDescriptor<T> | void => {
		const fn = target[propertyKey]
		// @ts-ignore
		return {
			value: debounce(function (this: any, ...args: any[]) {
				fn.call(this, ...args)
			}, seconds, options)
		}
	}
}
