import 'reflect-metadata'
import { createDecorator } from 'vue-class-component'
import { PropOptions } from 'vue'
import { Constructor, Vue } from 'vue-property-decorator'

var reflectMetadataIsSupported = typeof Reflect !== 'undefined' && typeof Reflect.getMetadata !== 'undefined'

function applyMetadata(options, target, key) {
	if (reflectMetadataIsSupported) {
		if (!Array.isArray(options) && typeof options !== 'function' && typeof options.type === 'undefined') {
			options.type = Reflect.getMetadata('design:type', target, key)
		}
	}
}

export function PropSync(options: (PropOptions | Constructor[] | Constructor) = {}): PropertyDecorator {
	// @ts-ignore
	return (target: Vue, key: string) => {
		applyMetadata(options, target, key)
		createDecorator(function (componentOptions, k) {
			// @ts-ignore
			const tmp = /sync(.*)/.exec(k)[1] as string
			const propName = tmp.charAt(0).toLowerCase() + tmp.slice(1)
			;(componentOptions.props || (componentOptions.props = {}))[propName] = options
			;(componentOptions.computed || (componentOptions.computed = {}))[k] = {
				get() {
					return this[propName]
				},
				set(value) {
					// @ts-ignore
					this.$emit(`update:${propName}`, value)
				}
			}
		})(target, key)
	}
}
