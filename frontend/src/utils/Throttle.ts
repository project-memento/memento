import { throttle } from 'lodash'

export function Throttle(seconds: number): MethodDecorator {
	return <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) : TypedPropertyDescriptor<T> | void => {
		const fn = target[propertyKey]
		// @ts-ignore
		return {
			value: throttle(function (this: any, ...args: any[]) {
				fn.call(this, ...args)
			}, seconds)
		}
	}
}
