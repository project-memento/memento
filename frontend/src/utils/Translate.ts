import 'reflect-metadata'
import { createDecorator } from 'vue-class-component'
import { Vue } from 'vue-property-decorator'
import i18n from '@/plugins/i18n'

function evaluateResult(vm, result) {
	if (typeof result === 'string') {
		// @ts-ignore
		return i18n.t(result)
	} else if (typeof result === 'function') {
		return wrapOrTranslate.call(vm, result)
	} else if (Array.isArray(result)) {
		return result.map(result => evaluateResult(vm, result))
	} else if (typeof result === 'object') {
		const entries = Object.entries(result)
		// @ts-ignore
		return Object.fromEntries(
			entries.map(([key, entry]) => {
				return [key, evaluateResult(vm, entry)]
			})
		)
	} else {
		return result
	}
}

function wrapOrTranslate(fn: (...any) => any) : (...any) => any {
	return function (this: any, ...args: any) {
		const result = fn.call(this, ...args)

		return evaluateResult(this, result)
	}
}

// TODO: add options to ignore given strings
export function Translate(path: string = '', options: any = {}): PropertyDecorator {
	// @ts-ignore
	return (target: Vue, key: string) => {
		createDecorator(function (componentOptions, k) {
			if (!componentOptions.computed) {
				componentOptions.computed = {}
			}
			if (k in componentOptions.computed) {
				// @ts-ignore
				componentOptions.computed[k].get = wrapOrTranslate(componentOptions.computed[k].get)
			} else {
				(componentOptions.computed || (componentOptions.computed = {}))[k] = {
					get() {
						// @ts-ignore
						return this.$t(path)
					}
				}
			}
		})(target, key)
	}
}
