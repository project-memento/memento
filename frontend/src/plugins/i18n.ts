import Vue from 'vue'
import VueI18n from 'vue-i18n'
import nb from '@/i18n/nb'
import en from '@/i18n/en'

Vue.use(VueI18n)

const messages = { en, nb }

const i18n = new VueI18n({
	locale: 'nb',
	fallbackLocale: 'en',
	messages,
	silentTranslationWarn: false
})

export default i18n

if (module.hot) {
	module.hot.accept(['../i18n/en', '../i18n/nb'], () => {
		i18n.setLocaleMessage('en', require('../i18n/en').default)
		i18n.setLocaleMessage('nb', require('../i18n/nb').default)
	})
}
