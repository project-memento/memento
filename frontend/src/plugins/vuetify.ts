import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
	theme: {
		primary: '#005461',
		secondary: '#00838F',
		accent: '#FFE83B',
		error: '#FF5252',
		info: '#2196F3',
		success: '#4CAF50',
		warning: '#FFC107',
		third: '#4DAEBB'

	},
	iconfont: 'fa',
})
