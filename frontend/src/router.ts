import Vue from 'vue'
import Router from 'vue-router'
import { createRouterLayout } from 'vue-router-layout'
import Component from 'vue-class-component'
import { vxm } from '@/store'

const RouterLayout = createRouterLayout(layout => {
	return import('@/layouts/' + layout + '.vue')
})

Vue.use(Router)

Component.registerHooks([
	'beforeRouteEnter',
	'beforeRouteLeave',
	'beforeRouteUpdate'
])

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: RouterLayout,
			children: [
				{
					path: '/',
					name: 'landing',
					component: () => import('./views/LandingPage.vue'),
				},
				{
					path: '/dashboard',
					name: 'dashboard',
					component: () => import('./views/Dashboard.vue'),
					meta: {
						requireAuth: true
					}
				},
				{
					path: '/profile',
					name: 'profile',
					component: () => import('./views/Profile.vue'),
					meta: {
						requireAuth: true
					}
				},
				{
					path: '/myposts',
					name: 'my-posts',
					component: () => import('./views/MyPosts.vue'),
					meta: {
						requireAuth: true
					}
				},
				{
					path: '/signup',
					name: 'sign-up',
					component: () => import('./views/SignUp.vue')
				},
				{
					path: '/signin',
					name: 'sign-in',
					component: () => import('./views/SignIn.vue')
				},
				{
					path: '/appointment',
					name: 'appointment',
					component: () => import('./views/Appointment.vue'),
					meta: {
						requireAuth: true
					}
				},
				{
					path: '/chat',
					name: 'chat',
					component: () => import('./views/Chat.vue'),
					meta: {
						requireAuth: true
					},
					children: [
						{
							path: ':conversationId',
							name: 'conversation',
							component: () => import('./views/chat/Conversation.vue'),
							meta: {
								requireAuth: true
							}
						}
					]
				},
				{
					path: '/admin',
					name: 'admin',
					component: () => import('./views/Admin.vue'),
					meta: {
						requireAuth: true
					},
					children: [
						{
							path: 'schools',
							component: () => import('./views/admin/Schools.vue'),
							meta: {
								requireAuth: true
							},
						},
						{
							path: 'categories',
							component: () => import('./views/admin/Categories.vue'),
							meta: {
								requireAuth: true
							},
						},
						{
							path: 'tags',
							component: () => import('./views/admin/Tags.vue'),
							meta: {
								requireAuth: true
							},
						}
					]
				},
			]
		},
	]
})

export default router

const loginStuff = async () => {
	await vxm.user.fetchLoggedInUser()
	await vxm.chat.fetchConversations()
}

router.beforeEach(async (to, from, next) => {
	if (vxm.auth.loggedIn) {
		if (vxm.user.me === null || typeof vxm.user.me === 'undefined') {
			await loginStuff()
		}
	}
	if (to.meta && to.meta.requireAuth) {
		if (vxm.auth.loggedIn) {
			next()
		} else {
			next({
				path: '/signin',
				query: {
					redirect: to.fullPath
				} })
		}
	} else {
		next()
	}
})

router.onReady(async () => {
	if (vxm.auth.loggedIn) {
		await loginStuff()
	}
})
