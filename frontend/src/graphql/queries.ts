import gql from 'graphql-tag'

export const SubscribeToMessages = gql`subscription {
	messages {
		id
		author {
			id
			profile {
				firstname
				lastname
				imageLink
			}
		}
		conversation {
			id
		}
		message
		sentAt
	}
}`
