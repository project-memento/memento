import { AxiosError, AxiosPromise, AxiosResponse } from 'axios'
import { ApiService } from '@/services/api.service'
import { TokenService } from '@/services/storage.service'
import { apolloClient, onLogin, onLogout } from '@/vue-apollo'
import gql from 'graphql-tag'
import { vxm } from '@/store'

export class AuthenticationError extends Error {
	errorCode: number

	constructor(errorCode : number, message : string) {
		super(message)
		this.name = this.constructor.name
		this.message = message
		this.errorCode = errorCode
	}
}

export class UserService {
	static async login(email : string, password : string) : Promise<string> {
		const response = await apolloClient.query({
			query: gql`query ($email: String!, $password: String!) {
				token: login(email: $email, password: $password)
			}`,
			variables: { email, password }
		})

		TokenService.refreshToken = response.data.token

		ApiService.mount401Interceptor()
		TokenService.token = response.data.token
		vxm.auth.loginSuccess(response.data.token)
		ApiService.setHeader()
		await onLogin(apolloClient, response.data.token)

		return response.data.token
	}

	static async refreshToken() {
		const refreshToken = TokenService.refreshToken

		try {
			const response = await ApiService.customRequest({
				method: 'post',
				url: '/api/auth/token',
				headers: {
					Authorization: `Bearer ${refreshToken}`
				}
			})

			TokenService.token = response.data.data.token
			ApiService.setHeader()

			return response.data.data.token
		} catch (error) {
			throw new AuthenticationError(error.response.status, error.response.data.detail)
		}
	}

	static async logout() {
		TokenService.clearRefreshToken()
		TokenService.clearToken()
		ApiService.removeHeader()
		await vxm.user.reset()

		await onLogout(apolloClient)

		ApiService.unmount401Interceptor()
	}
}
