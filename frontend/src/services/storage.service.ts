const TOKEN_KEY = 'access_token'
const REFRESH_TOKEN_KEY = 'refresh_token'

export class TokenService {
	static get token() : string | null {
		return localStorage.getItem(TOKEN_KEY)
	}

	static get refreshToken() : string | null {
		return localStorage.getItem(REFRESH_TOKEN_KEY)
	}

	static set token(value: string | null) {
		if (value === null) localStorage.removeItem(TOKEN_KEY)
		else localStorage.setItem(TOKEN_KEY, value)
	}

	static set refreshToken(value: string | null) {
		if (value === null) localStorage.removeItem(REFRESH_TOKEN_KEY)
		else localStorage.setItem(REFRESH_TOKEN_KEY, value)
	}

	static clearToken() {
		this.token = null
	}

	static clearRefreshToken() {
		this.refreshToken = null
	}
}
