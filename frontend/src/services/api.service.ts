import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'
import { TokenService } from '@/services/storage.service'
import { vxm } from '@/store'

export class ApiService {
	private static _401interceptor : number | null = null

	static init() {
	}

	static setHeader() {
		axios.defaults.headers.common['Authorization'] = `Bearer ${TokenService.token}`
	}

	static removeHeader() {
		axios.defaults.headers.common = {}
	}

	static get(resource : string) {
		return axios.get(resource)
	}

	static post(resource : string, data : any) {
		return axios.post(resource, data)
	}

	static put(resource : string, data : any) {
		return axios.put(resource, data)
	}

	static delete(resource : string) {
		return axios.delete(resource)
	}

	static customRequest(config : AxiosRequestConfig) {
		return axios(config)
	}

	static mount401Interceptor() {
		this._401interceptor = axios.interceptors.response.use(
			(response: AxiosResponse) => {
				return response
			},
			async (error : AxiosError) => {
				if (error.response && error.response.status === 401) {
					if (error.config.url && error.config.url.includes('/api/auth')) {
						vxm.auth.logout()
						throw error
					} else {
						try {
							await vxm.auth.refreshToken()

							return this.customRequest({
								method: error.config.method,
								url: error.config.url,
								data: error.config.data
							})
						} catch (e) {
							throw error
						}
					}
				}
				throw error
			}
		)
	}

	static unmount401Interceptor() {
		if (this._401interceptor !== null) {
			axios.interceptors.response.eject(this._401interceptor)
		}
	}
}

ApiService.init()
