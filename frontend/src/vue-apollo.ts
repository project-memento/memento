import Vue from 'vue'
import VueApollo from 'vue-apollo'
// @ts-ignore
import { ApolloClientClientConfig, createApolloClient, restartWebsockets } from 'vue-cli-plugin-apollo/graphql-client'
import ApolloClient from 'apollo-client'
import { vxm } from '@/store'

// Install the vue plugin
Vue.use(VueApollo)

// Name of the localStorage item
const AUTH_TOKEN = 'access_token'

// Http endpoint
const httpEndpoint = process.env.VUE_APP_GRAPHQL_HTTP || `//${window.location.host}/graphql`

// Config
const defaultOptions: ApolloClientClientConfig<any> = {
	// You can use `https` for secure connection (recommended in production)
	httpEndpoint,
	// You can use `wss` for secure connection (recommended in production)
	// Use `null` to disable subscriptions
	wsEndpoint: process.env.VUE_APP_GRAPHQL_WS || `${window.location.protocol.includes('https') ? 'wss' : 'ws'}://${window.location.host}/graphql`,
	// LocalStorage token
	tokenName: AUTH_TOKEN,
	// Enable Automatic Query persisting with Apollo Engine
	persisting: false,
	// Use websockets for everything (no HTTP)
	// You need to pass a `wsEndpoint` for this to work
	websocketsOnly: true,
	// Is being rendered on the server?
	ssr: false,
	fetchPolicy: 'network-only'

	// Override default apollo link
	// note: don't override httpLink here, specify httpLink options in the
	// httpLinkOptions property of defaultOptions.
	// link: myLink

	// Override default cache
	// cache: myCache

	// Override the way the Authorization header is set
	// getAuth: (tokenName) => ...

	// Additional ApolloClient options

	// Client local data (see apollo-link-state)
	// clientState: { resolvers: { ... }, defaults: { ... } }
}

// Call this in the Vue app file
export function createProvider(options = {}) {
	// Create apollo client
	const { apolloClient, wsClient } = createApolloClient({
		...defaultOptions,
		...options,
	})
	apolloClient.wsClient = wsClient

	// Create vue apollo provider
	const apolloProvider = new VueApollo({
		defaultClient: apolloClient,
		defaultOptions: {
			$query: {
				// fetchPolicy: 'cache-and-network',
			},
		},
		errorHandler(error) {
			// eslint-disable-next-line no-console
			console.log('%cError', 'background: red; color: white; padding: 2px 4px; border-radius: 3px; font-weight: bold;', error.Message)
		},
	})

	return apolloProvider
}

export const apolloProvider = createProvider()

export const apolloClient : ApolloClient<any> = apolloProvider.defaultClient

// Manually call this when user log in
export async function onLogin(apolloClient : any, token : any) {
	if (typeof localStorage !== 'undefined' && token) {
		localStorage.setItem(AUTH_TOKEN, token)
	}

	if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient)
	try {
		await apolloClient.resetStore()
		await vxm.user.fetchLoggedInUser()
	} catch (e) {
		// eslint-disable-next-line no-console
		console.log('%cError on cache reset (login)', 'color: orange;', e.Message)
	}
}

// Manually call this when user log out
export async function onLogout(apolloClient : any) {
	if (typeof localStorage !== 'undefined') {
		localStorage.removeItem(AUTH_TOKEN)
	}
	if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient)
	try {
		await apolloClient.resetStore()
	} catch (e) {
		// eslint-disable-next-line no-console
		console.log('%cError on cache reset (logout)', 'color: orange;', e.Message)
	}
}
