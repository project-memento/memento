module.exports = {
	extends: [
		'plugin:vue/recommended',
		'@vue/standard',
		'@vue/typescript',
		'../.eslintrc.js'
	],
	parser: 'vue-eslint-parser',
	parserOptions: {
		parser: '@typescript-eslint/parser',
		ecmaVersion: 2018,
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true
		}
	},
	rules: {
		'vue/max-attributes-per-line': [
			'error',
			{
				singleline: 5,
				multiline: {
					max: 1,
					allowFirstLine: false
				}
			}
		]
	},
	overrides: [
		{
			files: ['**/*.spec.ts'],
			parserOptions: {
				parser: '@typescript-eslint/parser',
				sourceType: 'module'
			},
			env: { jest: true },
			globals: {
				mount: false,
				shallowMount: false,
				shallowMountView: false,
				createComponentMocks: false,
				createModuleStore: false
			}
		},
		{
			// TODO: enable again when Prettier 1.17 launches
			files: ['*.vue'],
			plugins: ['sort-class-members'],
			rules: {
				// https://github.com/mysticatea/vue-eslint-parser#%EF%B8%8F-known-limitations
				'max-len': 'off',
				// Other
				indent: 'off',
				'vue/script-indent': ['error', 'tab', { baseIndent: 1 }],
				'vue/html-indent': ['error', 'tab'],
				'vue/no-multi-spaces': 'error',
				'vue/component-name-in-template-casing': ['error', 'PascalCase', {
					'registeredComponentsOnly': false,
					'ignores': []
				}],
				'vue/attributes-order': 'off',
				'sort-class-members/sort-class-members': [
					'error',
					{
						order: [
							'[properties]',
							'[accessor-kinds]',
							'[lifecycle-methods]',
							{ type: 'method' },
							{
								name: 'render',
								type: 'method'
							}
						],
						groups: {
							'accessor-kinds': [{ type: 'method', kind: 'get' }, { type: 'method', kind: 'set' }, '[accessor-pairs]'],
							'lifecycle-methods': [
								{ name: 'beforeCreate', type: 'method' },
								{ name: 'created', type: 'method' },
								{ name: 'beforeMount', type: 'method' },
								{ name: 'mounted', type: 'method' },
								{ name: 'beforeUpdate', type: 'method' },
								{ name: 'updated', type: 'method' },
								{ name: 'activated', type: 'method' },
								{ name: 'deactivated', type: 'method' },
								{ name: 'beforeDestroy', type: 'method' },
								{ name: 'destroyed', type: 'method' }
							]
						}
					}
				]
			}
		}
	]
}
