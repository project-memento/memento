const path = require('path')

module.exports = {
	displayName: 'Frontend',
	moduleFileExtensions: [
		'js',
		'jsx',
		'json',
		'vue',
		'ts',
		'tsx'
	],
	transform: {
		'^.+\\.vue$': 'vue-jest',
		'.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
		'^.+\\.tsx?$': 'ts-jest'
	},
	globals: {
		'ts-jest': {
			'disableSourceMapSupport': true
		}
	},
	moduleNameMapper: {
		'^@/(.*)$': 'src/$1'
	},
	snapshotSerializers: [
		'jest-serializer-vue'
	],
	testMatch: [
		'<rootDir>/tests/unit/**/*.spec.(js|jsx|ts|tsx)'
	],
	testURL: 'http://localhost/',
	coverageReporters: [
		'json'
	],
	collectCoverageFrom: [
		'src/**/*.(ts|vue)',
		'!**/node_modules/**',
		'!src/main.ts',
		'!src/App.vue',
		'!src/**/*.d.ts',
	]
}
