#!/usr/bin/env node

process.env.NODE_ENV = 'test'

const startBackend = require('../../backend/scripts/e2e').default
const Service = require('@vue/cli-service/lib/Service')
const service = new Service(process.env.VUE_CLI_CONTEXT || process.cwd())
const { info, chalk, execa, error } = require('@vue/cli-shared-utils')

async function start() {
	const rawArgs = process.argv.slice(2)
	const args = require('minimist')(rawArgs, {
		boolean: [
			// build
			'modern',
			'report',
			'report-json',
			'watch',
			// serve
			'open',
			'copy',
			'https',
			// inspect
			'verbose'
		]
	})
	removeArg(rawArgs, 'headless', 0)
	removeArg(rawArgs, 'mode')
	removeArg(rawArgs, 'url')
	removeArg(rawArgs, 'config')

	info('Starting e2e tests..')
	const { url: backendUrl, server: backendServer } = await startBackend()
	info(`Backend started on: ${backendUrl}`)
	process.env.BACKEND_HOST = backendUrl
	process.env.VUE_APP_GRAPHQL_WS = backendUrl.replace('http://', 'ws://') + 'graphql'
	const { url, server } = await service.run('serve', { mode: args.mode || 'production' })

	const configs = typeof args.config === 'string' ? args.config.split(',') : []
	const cyArgs = [
		args.headless ? 'run' : 'open',
		'--config',
		[`baseUrl=${url}`, ...configs].join(','),
		...rawArgs
	]

	const cypressOptions = {
		config: {
			baseUrl: url,
			...configs
		}
	}
	const cypressBinPath = require.resolve('.bin/cypress')
	const runner = execa(cypressBinPath, cyArgs, { stdio: 'inherit' })
	if (server) {
		runner.on('exit', () => backendServer.httpServer.close())
		runner.on('error', () => backendServer.httpServer.close())
		runner.on('exit', () => server.close())
		runner.on('error', () => server.close())
	}

	if (process.env.VUE_CLI_TEST) {
		runner.on('exit', code => {
			process.exit(code)
		})
	}

	return runner
}

start().catch(err => {
	error(err)
	process.exit(1)
})

function removeArg(rawArgs, arg, offset = 1) {
	const matchRE = new RegExp(`^--${arg}`)
	const equalRE = new RegExp(`^--${arg}=`)
	const i = rawArgs.findIndex(arg => matchRE.test(arg))
	if (i > -1) {
		rawArgs.splice(i, offset + (equalRE.test(rawArgs[i]) ? 0 : 1))
	}
}
