// https://docs.cypress.io/api/introduction/api.html

import EntityFixtures from '../../../../backend/src/fixtures/EntityFixtures'
import { School } from '../../../../backend/src/entities/School'
import { StudentYear } from '../../../../backend/src/entities/StudentYear'
import { getApp } from '../support/utils'
const fixtures = new EntityFixtures()

describe('SignUp', () => {
	it('Can sign up new user', async () => {
		cy.task('clean:db')
		let school
		let t
		cy.task('fixtures', fixtures.School())
			.then((_school: School) => {
				school = fixtures.deserialize(_school)
				cy.visit('/signup')
				getApp().then(app => {
					t = app.$t.bind(app)
				})
			}).then(() => {
				cy.get('input[name="firstName"]').type('Ola')
				cy.get('input[name="lastName"]').type('Nordmann')
				cy.get('input[name="email"]').type('test@student.westerdals.no')
				cy.get('input[name="password"]').type('MegaS3cure')
				cy.get('input[name="confirmPassword"]').type('MegaS3cure')
				cy.contains('button', 'Next step').click()

				cy.get('input[data-test="fieldOfStudy"]')
					.parents('.v-select').click()
				cy.get('.menuable__content__active')
					.contains('.v-list__tile', school.__fieldsOfStudies__[0].name).click()
				cy.get('input[data-test="studyYear"]')
					.parents('.v-select').click()
				cy.get('.menuable__content__active')
					.contains('.v-list__tile', t('studyYear.' + StudentYear[StudentYear.SECOND_YEAR])).click()
				cy.contains('button', 'Next step').click()
				cy.get('input[data-test="acceptTerms"]')
					.parents('.v-input--selection-controls').click()
				cy.contains('button', 'Next step').click()

				cy.contains('button', 'Register').click()
				cy.location('pathname')
					.should('eq', '/dashboard')
			})
	})
})
