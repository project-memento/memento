// @ts-ignore
export const getStore = () => cy.window().its('__app__.$store')

// @ts-ignore
export const getApp = () => cy.window().its('__app__')
