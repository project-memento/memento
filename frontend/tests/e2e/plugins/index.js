const webpack = require('@cypress/webpack-preprocessor')
const axios = require('axios')

module.exports = (on, config) => {
	on('file:preprocessor', webpack({
		webpackOptions: {
			node: {
				fs: 'empty'
			},
			resolve: {
				extensions: ['.ts', '.txs', '.mjs', '.js']
			},
			module: {
				rules: [
					{
						test: /\.tsx?$/,
						use: [
							{
								loader: 'babel-loader',
								options: {
									presets: ['@babel/preset-env'],
								},
							},
							{
								loader: 'ts-loader',
								options: { transpileOnly: true },
							}
						]
					}
				]
			}
		},
		watchOptions: {}
	}))

	on('task', {
		'clean:db'() {
			return axios.post(`${process.env.BACKEND_HOST}graphql`, {
				operationName: null,
				variables: {},
				query: 'mutation { cleanDB }'
			}).then(response => response.data.data.cleanDB)
		},
		'fixtures'(fixture) {
			return axios.post(`${process.env.BACKEND_HOST}graphql`, {
				operationName: null,
				variables: {
					fixtures: JSON.stringify(fixture)
				},
				query: 'mutation ($fixtures: String!) { fixtures(fixtures: $fixtures) }'
			})
				.then(response => JSON.parse(response.data.data.fixtures))
		}
	})

	return Object.assign({}, config, {
		watchForFileChanges: true,
		env: {
			CI: process.env.CI
		},
		video: false,
		viewportWidth: 1280,
		viewportHeight: 720,
		waitForAnimations: true,
		animationDistanceThreshold: 4,
		defaultCommandTimeout: 4000,
		execTimeout: 60000,
		pageLoadTimeout: 60000,
		requestTimeout: 5000,
		responseTimeout: 30000,
		supportFile: 'tests/e2e/support/setup.ts',
		integrationFolder: 'tests/e2e/specs',
		fixturesFolder: 'tests/e2e/fixtures',
		screenshotsFolder: 'tests/e2e/screenshots',
	})
}
