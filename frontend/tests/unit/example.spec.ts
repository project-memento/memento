import { shallowMount } from '@vue/test-utils'
import { CreateElement } from 'vue'

describe('HelloWorld.vue', () => {
	it('renders props.msg when passed', () => {
		const msg = 'new message'
		const wrapper = shallowMount({
			props: ['msg'],
			render(h:CreateElement) {
				// @ts-ignore
				return h('p', [this.msg])
			}
		}, {
			propsData: { msg }
		})
		expect(wrapper.text()).toMatch(msg)
	})
})
