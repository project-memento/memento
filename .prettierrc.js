module.exports = {
	printWidth: 150,
	useTabs: true,
	semi: false,
	singleQuote: true,
	trailingComma: 'none',
	bracketSpacing: true,
	jsxBracketSameLine: false,
	arrowParens: 'avoid',
	proseWrap: 'never',
	htmlWhitespaceSensitivity: 'strict',
	endOfLine: 'lf'
}
