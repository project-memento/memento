#!/bin/bash
helm upgrade --install \
      --wait \
      --set releaseOverride="review-25-finish-mrhs14" \
      --set image.repository="registry.gitlab.com/project-memento/memento/25-finish-configuring-ci-cd" \
      --set image.tag="044ec1aa3e0d1eaa2318180a981926a25406657a" \
      --set image.pullPolicy=IfNotPresent \
      --set image.secrets[0].name="gitlab-registry" \
      --set application.track="stable" \
      --set service.commonName="le.connect.university" \
      --set service.url="review-25-finish-mrhs14.connect.university" \
      --set replicaCount="1" \
      --namespace="memento" \
      "review-25-finish-mrhs14" \
      ./memento

#helm delete --purge "review-25-finish-mrhs14"
