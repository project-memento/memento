#!/usr/bin/env node

const NYC = require('nyc/index')
const libCoverage = require('istanbul-lib-coverage')
const libReport = require('istanbul-lib-report')
const reports = require('istanbul-reports')
const path = require('path')
const fs = require('fs')
const makeDir = require('make-dir')

const outputFile = path.join(__dirname, '../coverage/coverage.json')
const outputDir = path.dirname(outputFile)
process.env.NYC_CWD = process.cwd()
const nyc = new NYC({
	reporter: ['html', 'text'],
	instrumenter: './lib/instrumenters/noop'
})
nyc.cleanup()

makeDir.sync(outputDir)

const map = libCoverage.createCoverageMap({})

nyc.eachReport(
	undefined,
	report => {
		map.merge(report)
	},
	path.join(__dirname, '../backend/coverage')
)

nyc.eachReport(
	undefined,
	report => {
		map.merge(report)
	},
	path.join(__dirname, '../frontend/coverage')
)

map.data = nyc.sourceMaps.remapCoverage(map.data)

fs.writeFileSync(outputFile, JSON.stringify(map, null, 2), 'utf8')

const context = libReport.createContext({
	dir: outputDir,
	watermarks: nyc.config.watermarks
})

tree = libReport.summarizers.pkg(map)

nyc.reporter.forEach(_reporter => {
	tree.visit(
		reports.create(_reporter, {
			skipEmpty: nyc.config.skipEmpty,
			skipFull: nyc.config.skipFull
		}),
		context
	)
})
