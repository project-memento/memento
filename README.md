# Memento [![pipeline status](https://gitlab.com/project-memento/memento/badges/master/pipeline.svg)](https://gitlab.com/project-memento/memento/commits/master) [![coverage report](https://gitlab.com/project-memento/memento/badges/master/coverage.svg)](https://gitlab.com/project-memento/memento/commits/master)

Nettsiden vil kjøre på [https://connect.univeristy/](https://connect.univeristy/) frem til sensurfrist, med forbehold om nedetid og eller at prøveperioden hos Azure går ut. Der ligger det data i systemet fra før av. Og man kan lage en bruker ved å registrere den med en epost som slutter på `@student.kristiania.no`

For å kjøre alt sammen lokalt må du ha docker installert. Da trenger du bare kjøre `docker-compuse up` inne i mappen, og da vil den automatisk bygge løsningen og laste ned alle dependencies. I tillegg til at den vil starte en postgres server. Om du allerede har en postgres server kjørende på port 5432 kan du kommentere ut linje 6 i `docker-compose.yml`. Når den er startet kan du logge inn med `admin@connect.university` og passord `supersecure` Da kan du gå i admin menyen og legge inn skole, kategorier og tags.

For å kunne lage nye brukere må du opprette en skole som har en email suffix.
